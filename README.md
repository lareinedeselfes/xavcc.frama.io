# ⋅ [Indigo](http://sergiokopplin.github.io/indigo) Minimalist Jekyll Template ⋅

***

1. [What has inside](#what-has-inside)
2. [Setup](#setup)
3. [Settings](#settings)
4. [With your own custom domain](#with-your-own-custom-domain)
   + Use Let's Encrypt
5. [How to](#how-to)
6. [To do](#to-do)

![](https://framagit.org/Xavcc/xavcc.frama.io/raw/master/assets/screen-shot.png)

***

## What has inside

- [Jekyll](https://jekyllrb.com/), [Sass](http://sass-lang.com/) ~[RSCSS](http://rscss.io/)~ and [SVG](https://www.w3.org/Graphics/SVG/)
- Page fully loaded after 495 ms.

|Info |	Offset |Duration |
|-----|--------|---------|
|Redirect |	0 |	0 |
| App cache| 0 | 87 |
| DNS lookup | 87 | 39 |
| TCP connection | 126 | 80 |
| TCP request | 206 | 38 |
| TCP response | 244 | 61 |
| Processing | 261 	| 234 |
| onload event | 495 | 0 |

with apptelemetry.com
- Google Speed: [98/100](https://developers.google.com/speed/pagespeed/insights/?url=http%3A%2F%2Fsergiokopplin.github.io%2Findigo%2F); but #[FuckOffGoogle](https://wiki.fuckoffgoogle.de/)
- Tests with [gitlab Continuous Integration & Delivery ](https://about.gitlab.com/product/continuous-integration/)
- Free font, [libre Calson Text](https://fontlibrary.org/en/font/libre-caslon-text)
- Use [RSS](https://xavcc.frama.io/feed.xml)

## Setup

0. :star: to the project. :metal:
2. Fork this version of [Indigo](https://framagit.org/Xavcc/xavcc.frama.io/tree/master)
3. Add  [`.gitlab-ci.yml`](https://framagit.org/Xavcc/xavcc.frama.io/blob/master/.gitlab-ci.yml) and configure it
3. Edit `_config.yml` with your data (check <a href="README.md#settings">settings</a> section)
   + Use Username.gitlab.io or UserName.frama.io
   + Use your own custom domain
4. Write some posts :bowtie:

If you want to test locally on your machine, do the following steps also:

1. Install [Jekyll](http://jekyllrb.com), [NodeJS](https://nodejs.org/) and [Bundler](http://bundler.io/).
2. Clone the forked repo on your machine
3. Enter the cloned folder via terminal and run `bundle install`
4. Then run `bundle exec jekyll serve --config _config.yml,_config-dev.yml`
5. Open it in your browser: `http://localhost:4000`
6. Test your app with `bundle exec htmlproofer ./_site`
7. Do you want to use the [jekyll-admin](https://jekyll.github.io/jekyll-admin/) plugin to edit your posts? Go to the admin panel: `http://localhost:4000/admin`. The admin panel will not work on GitHub Pages, [only locally](https://github.com/jekyll/jekyll-admin/issues/341#issuecomment-292739469).

## Settings

You must fill some informations on `_config.yml` to customize your site.

```
name: John Doe
bio: 'A Man who travels the world eating noodles'
picture: 'assets/images/profile.jpg'
...

and lot of other options, like width, projects, pages, read-time, tags, related posts, animations, multiple-authors, etc.
```

## With your own custom domain 

**Redirect to the HTTPS version**

Unfortunately, unlike our FsPages installation, sites are not redirected to the HTTPS version if there is one (not even for frama.io subdomains, which automatically benefit from an HTTPS version). However, a [ticket](https://gitlab.com/gitlab-org/gitlab-ce/issues/28857) is available at Gitlab on this subject.

In the meantime, to overcome this problem, you can include this little piece of javascript in your HTML pages:
```
<script>
    var loc = window.location.location.href+'';
    if (loc.indexOf('http://')==0){
        window.location.href = loc.replace('http://','https://');
    }
</script>
```

### Use Let's Encrypt

The use of [Let's Encrypt certificates](https://letsencrypt.org/) is not easy but a [ticket](https://gitlab.com/gitlab-org/gitlab-ce/issues/28996) is available at Gitlab to integrate Let's Encrypt directly into the Gitlab Pages.


## How To

Check the [FAQ](./FAQ.md) if you have any doubt or problem.

## To Do

+ Correct w3c mistakes in [NU thlm checker](https://validator.w3.org/nu/?doc=https%3A%2F%2Fxavcc.frama.io%2F)
+ Add a Riot/matrix Icon with hyperlink in social contact

---

Content  licence libre CC BY SA 4.0 ©Xavier Coadic 2018
Code : [MIT](http://kopplin.mit-license.org/) License © Sérgio Kopplin
