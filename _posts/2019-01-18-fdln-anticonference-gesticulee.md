---
title: "Fdln, préparation d'une anticonférence gesticulée"
layout: post
date: 2019-01-18
image: /assets/images/fixingabrain.png
headerImage: True
tag:
- hsociety
- Rennes
- Numérique
category: blog
author: XavierCoadic
description: recette de préparation (#culturepop ?)
---

Comme promis dans un [récent article](/fdln-2019), et avec 2 ou 3 jours de retard, je publie aujourd'hui _ma recette_ de préparation d'une anticonférence gesticulée.
La thématique de cette soirée du le 4 février est « Biononymous & Bioprivacy , au Mille Potes à 19h, bd de la liberté à Rennes, prix libre.

En résumé de cette recette libre : j'ai beaucoup bossé et je bosse encore, j'ai distribué des affiches pour communiquer, j'ai fait appels à des ami⋅e⋅s.

## Conférence gesticulée c'est quoi ?

C'est un format de conférence d'éducation populaire conçu par Franck Lepage[^1] en 2006. Un hybride entre le spectacle et la conférence, qui vise à produire du savoir politique, à donner des clés d’analyse et permettre d’aller plus loin grâce aux ateliers qui s’ensuivent. C'est une expression directe transgresse la légitimité (toujours contestable) à parler en public mobiilsants des savoirs froids et des savoirs chauds dans une tentative d'inclusion du public aux processus de réflexion intellectuelle et vivant, ainsi que l'inclusion des les possibilités d'actions.

Il s'agit donc pour la ou les personnes qui réalisent cet exercice de mettre en scène et surtout en œuvre, c'est à dire en réalité opérante et agissante, cela :
+ Les choses vécues 
+ Les choses comprises 
+ Les choses apprises

Les savoirs « chauds » sont à considérer tels que :  savoirs « illégitimes »,  savoirs populaires,  savoirs politiques, savoirs de l’expérience, savoirs utiles pour de l’action collective, d’où l’idée « d’inculture »[^2], ou encore de « conte politique non autorisé ». S’il faut faire partie du CNRS pour être autorisé à poser une parole publique en France sur un sujet, du coup, ce qu’on a compris pendant 20 ans d’activité ne vaut pas grand-chose et n’a que le statut méprisé d’« états d’âme ».

Les savoirs « froids » : L’université publie d’excellentes analyses politiques, sociologiques, sur tous les sujets dont nous avons besoin. Bourdieu sur la culture du capitalisme, Meredith Patterson sur le bioPunk Manifesto, Habermas sur l'idéologie de la science et de la technique, Cioran sur les prophètes. Pourquoi et comment ces savoirs ne servent que très très peu dans la mobilisation et l’action collective ? Les « actrices » et « acteurs » sociaux ne lisent pas ou peu la production des intellectuel⋅le⋅s, qui elle-même ne rencontre pas ou peu le travail des acteurs sociaux.

La propostion de valeur de ce format est celle d’une transmission, **jamais** autorisée, jamais organisée : la transmission de l’expérience collective, (c'est-à-dire politique) que nous emmagasinons au fil de nos expériences. Le savoir c'est la connaissance passé aux feux de l'expérience. 

> « _Néanmoins les conférences gesticulées ne sont ni des « appels », ni des « manifestes », ni des « tribunes » : elles relèvent d’un répertoire d’action spécifique. Par les formes qu’elles empruntent, elles ne doivent pas être dissociées du vaste champ de l’éducation populaire et du théâtre politique dont elles peuvent être appréhendées comme l’un des multiples formats : les conférences gesticulées s’intègrent en ce sens dans diverses expressions qui croisent le théâtre interactif, le théâtre forum, le théâtre-action, le théâtre d’intervention, le théâtre de l’opprimé, le théâtre de rue, et plus globalement le théâtre militant. Dans ce vaste champ, les conférences gesticulées se singularisent par la façon dont elles articulent « intime » et « politique_ ». »[^3]

## Une anticonférence gesticulée c'est quoi ?

Je ne vise pas un concevoir un format qui s'oppose conflictuellement à celui des conférences gesticulée. Je considère le format _confèrence getsiculée_ comme excellent et porteur de valeurs fortes et utiles. Je reconnais dans cette méthode de préparation et de réalisation nombres de points de grande qualité et un environnement apprenant dans le partage et le plaisir. La _méthode conférence gesticulée_ me plaît, cependant je souhaite pousser plus loin et plus librement l'expérience _hors des sentiers_ de conférences, comme [évoqué aupravant](/fdln-2019). Des conférences j'en ai réalisée plusieurs, j'ai aussi tenté d'autre format comme le [cercle samoan](/casper-sudweb-2018).

Je décide donc de partir de la _matrice_ de la conférence gesticulée, de son noyau de fonctionnement, pour écrire un autre _logiciel_ de conception et de réalisation, basé sur la conférence getsiculée, qui se pose _face à_ la configuration conférence. Un _fork the world_[^3] comme nous disons avec des ami⋅e⋅s. J'ai ausi pris le parti de dessiner les plus de temps et d'énergie possibles aux ateliers et aux moments non-formels.

## Comment je me prépare

> _TL;DR : l'écriture du contenu n'arrive qu'à la toute fin du processsus_

Pour trouver des mots et lignes de guides à ce travail de préparation je suis allé, entre autres, sur les sites internets :
+ <https://www.ardeur.net>
+ <https://www.scoplorage.org>
+ <https://www.scoplepave.org>
+ <https://conferences-gesticulees.be>
+ <https://informatique-ou-libertes.fr>
+ <https://clap-lodeve.com/fr/conference-gesticulee>
+ <https://www.lecontrepied.org>

### Voir par soi-même

_Ou l'importance d'observer_

Depuis 2012, j'ai été spectateur de plusieurs conférences gesticulées sur diverses thématiques à Marseille, Saint-Étienne, Rennes, en centre Bretagne, ou à Paris. Féminisme et non-mixité, indépendantisme et emprisonnement politique, écologie et radicalité, gay savoir, paysanerie du déshonneur à la fierté... Ce fut à chaque fois un grand moment d'apprentissage et d'émotions.

J'ai essayé le plus possible de discuter ensuite avec les conférencières et confèrenciers sur les _pourquoi_ de leurs choix, sur les _comment_ de leur conférence gesticulée ; et de demander quelques astuces issues de leur vécu d'apprentissage. J' en retiens : 

+ Assumer ses peurs et ses faiblesses
+ Travailler en amont hors de sentiers balisés
+ Répéter son format mais laisser toute place à l'improvision (ce qui se prépare beaucoup en fait)
+ Faire confiance au public

J'ai aussi regardé de nombreuses vidéos en ligne, parfois plusieurs les mêmes en boucle. J'ai lu pas mal, du billet blog jusqu'au _papier_ de recherche.

J'ai essayé de faire maturer, macérer, tout cela en travaillant aussi sur la fermentation du moût de mes raisins (mes sujets de prédilections, mes angoisses, mes envies...). Puis j'ai formulé un postulat.

### Émettre son hypothèse

_Ou phase péparatoire à l'expérimentation_

Formuler une hypothèse ne tombe forcemment comme une pomme sur une tête après une sieste sous un arbre...

Après les retours d'expériences exterieurs sur mes conférences au format classique, après mise sur papier de mes ressentis....

Il n'est pas non plus évident qu'une hypothèse soit le fait d'une personne isolée...

Après longues discussions avec des personnes qui vont au fond des choses, telles que ClaireZed, OncleTom, Pntbr, Thomas Wolff, Noémie, Newick, Anachitect, Maxlath, Sylvia, Tonton Duriaux, Ryu5t & Ch4rizz, Natouille... et d'autres...

Après tout ce temps et ces rencontres, après ces matériaux et morceaux, j'ai formulé une hypothèse :

(JFDIY) **Une anticonférence gesticulée à laquelle le public participe activement**

Pas de conférencière ou de conférencier sur une scène. C'est un atelier alors ? Peut-être... Ou pas...

### Dessiner son plan de travail

_Concevoir un plan_

J'ai alors commencé à mettre noir sur blanc ce que je devais franchir comme étapes et comment les parcourir.

Le sujet « Biononymous & Bioprivacy » je pense le connaître assez pour produire du contenu.

**1 – Le contenu doit comporter** :

• Un récit personnel, des anecdotes autobiographiques, qui illustrent et rendent « véridiques » les analyses. Le puissance de l’anecdote est réel.

• Un commentaire politique analysé du problème en question (les savoirs « chauds ») « ce que j’ai compris moi-même ». Mes réflexions.

• Des apports extérieurs universitaires  sur la question  les  « savoirs froids » « ce que d’autres en ont dit ». On apprend quelque chose.

• Une dimension historique : l’historicité c’est le rappel de la marge de manœuvre, c’est de comprendre comment le problème s’est construit, c'est apporter des perspectives au regard du passé.

Je sais que je dois franchir ces étapes, j'ai certainement beaucoup de matières à y placer avec grande sagacité. Je dois garder cela en tête pour la fin du processus de conception et me concentrer avec autant d'efforts sur la forme qui supportera ce contenu et les _exigences_ de l'anticonférence gesticulée.

**2 – la forme** :

Je cherche à raconter des histoires vécues qui font _cogiter_ (Cogito ergo sum) en y apportant mes éclairages et mes prolongements (vers l’action), mais aussi ceux du public que je souhaite particpant actif. Cette forme naît dans les conventions du spectacle. Pour autant elle n’est pas du « théâtre » aux formes strictes. Il s’agit d'assumer un moment militant subjectif, radical. Cette forme qui accouche d'un moment collectif qui vise à communiquer des émotions : joie, colère ou enthousiasme, tristesse ou amertume. Il faut se mettre en posture de partage de l'intime, offrir de soi pour recevoir en retour. La forme choisie ne se limite pas à la version intellectualisée du contenu. _L'expert⋅e froid⋅e_ peut être mis à la dérision dans cette configuration. Par exemple, je peux choisir de démarrer ce moment par l'immersion de une intimité, qui m'ait propore mais peut aussi être commune à des personnes présentes, avec une scène d'introduction forte par confidence sur un vécu.

## Comment travailler ?

_Mettre en étapes son expérience_

Choissisez un sujet que vous maîtrisez pour gérer le _libre et franc parler_.

Choississez un lieu approprié pour faire _agora_, comme un café pour faire tiers-lieu[^6]. J'ai choisi un bar où je me sens bien, dans lequel nous déjà fait des dizaines de _choses_ libres et populaires, qui portent plusieurs symboles comme son nom « Mille Potes » et son adresse « Bd de la Liberté ».

Pour le sujet, aborder des souvenirs ancrés dans une pratique professionnelle longue est favorable à votre aisance. Par exemples, une infirmière ou un infirmier peut parler sans trop préparer, ni forcer, du rapport en corps et soins.

Vous pouvez aussi choisir un sujet dont il vous tient à cœur de parler sans en avoir une longue expérience. Vous devrez alors fournir des efforts considérables d'acquisitation d'informations, d'interview, d'enquêtes, de documentation et de lectures universitaires. Un Evrest à escalader en plus du travail de préparation de votre format.

J'ai choisi de stimuler mes sens et souvenirs dans de nombreuses étapes à suivre avec écoute de musiques, y compris celles que j'écoutais il y 15 ans, et relecture du roman « La zone du Dehors », qui m'inspire beaucoup depuis plus de 10 ans. Ceci étant voulu pour favoriser la remontée de souvenirs et d'émotions, mais aussi des sensations de _transe_ lors de phase de créativité.

### Créer un archipel

Identifier et lister les « bouts » de trucs à dire. Ce que vous voulez absolument aborder ou dire précisemment ! Ne pas chercher un ordre ou des transitions, cela adviendra plus tard dans le processus de construction d’un « récit ». Ça, c'est pour la fin de la création.

Établir une liste ! S’aider pour cela en pensant à des choses qui vous sont fréquentes : des anecdotes, des bouts de théorie, etc… qui constituent votre discours et noter les fonds militant, noter des composants de votre culture.

J'avoue volontiers avoir du mal à _parler de moi_, de livrer de mon intime et de laisser paraître de mon naturel, tout en ayant aussi crainte de mon artificialité[^7].

Pour faire cette étape d'archipel j'en procédé en 4 temps :

1. Un travail de _jet de petits bouts de trucs à la vollée_ sur feuille blanche dans un endroit inspirant où je me sens en sécurité, en étant seul. Plutot un moment de pré-préparation
2. Je me suis fait aider par mon amie pour verbaliser les _choses_ et écrire (c'est en plus un chouette moment de couple de parler de l'intime issu du vécu professionnel). Dans une discussion, elle m'aide à :
    + Exprimer ce qui me tient à cœur
    + Dire ce qui m'a marqué
    + Détailler parfois plus ce que je cherche, plus ou moins consciemment, à éluder
    + Une feuille de papier par île qui représente un _trucs à dire_
    + Ni ordre, ni classement, ni arrangement entre les îles
    + Des post-it pour chaque anecdote, posés en vrac sur une table
    + Des post-it pour chaque théorie, en vrac sur table
3. Un temps similaire au `2.` mais seul comme pour `1.` avec les musiques qui m'inspirent ou après de longues séances de lecture
4. Comparaison entre `2.` et `3.` pour réperer ce qui avait été oublié, relever ce qui revient plusieurs fois (insistance)


Par exemple, j'ai _mis_ pas mal de _choses_ provenant de ma carrière chez les Marins pompiers de Marseille, aussi une anecdote sur 12 accouchements ; ou encore ma découverte du biohacking, l'approriation du Vivant jusque dans l'intimité génétique ; des anecdotes avec mon pére et la phrase « je n'ai rien à cacher » ; mon rapport à l'informatique et les logiciels libres. 

### Méler et métaphorer

Cette étape vise à rendre _parlant_ des concepts que vous abordez par deux bords, ou plus.

Chercher sur certains points à mêler deux histoires qui n’ont apparemment rien à voir et tenter une métaphore. 
Exemple :
- Les empreintes de doigts et les bacteries que vous portez
- La désobéissance et le jardinage
- Les rond-points et les tiers-lieux

Là encore, je me suis beaucoup servi de « La Zone du Dehors », mais également du « Cheval D'orgeuil » de Jakez Hélias<sup>[Wikipedia](https://fr.wikipedia.org/wiki/Pierre-Jakez_H%C3%A9lias)</sup>, également de mon tour de Bretagne en 2015 et tour de France en 2016 `#Labose`. Je vais aussi piocher ches Lovevraft<sup>[Wikipedia](https://fr.wikipedia.org/wiki/Howard_Phillips_Lovecraft)</sup> ou encore revisionner tout un tas de fois les émissions Bits d'Arte. 

### Trouver un démarrage

Entrer directement dans une intimité partageable ? C'est le but recherché par le format _conférence gesticulée_.

La situtation première entraîne souvent la situation générale : « Tiens, si je montrais comment on m'a dit de ne pas faire une conférence getsiculée ? » ou « Si je commençais par une scène de massage cardiaque (réanimation cardio-pulmonaire) ? ».

Vous pouvez écrire plusieurs démarrage et ensuite les tester devant un petit groupe d'ami⋅e⋅s devant lesquel vous aurez moins de craintes que devant un plus large public.

Pour ma part, ayant décidé de faire au maximun participer le publique, je prévois de multiples (3 ou 4) démarrages qu'elles et ils pourraient choisir dès le début et qui influenceront le déroulé de la suite.

Je tends vers un scénarisation très minamiliste pour laisser place et temps à la participation active et à l'improvisation. 

### Trouver une fin

Définissez avec soin le message de conclusion sur lequel vous voulez faire _attérir_ le public. Cela vous permettra d'enchainer sur des temps avec plus de proximité et de pratiques pour les ateliers d'après _anticonférence gesticulée_.

### Trouver des transitions 

Il est temps de relier les îles en Archipel et faire émerger un récit. Pour faire cela, je reste sur les pas du modèle _conférence gesticulée_.

1. Récit de vie : analyser son parcours de manière critique et collective ;
2. Recherche-action : lire et étudier collectivement, s’approprier des concepts ;
3. Collectiviser de l’individuel : parler de soi publiquement, assumer une position ;
4. Entraînement mental : nommer les problèmes, proposer des analyses ;
5. Mise en corps des idées : des anecdotes, pratiquer la théorie incarnée ;
6. Travailler l’émotion politique : mobiliser ses affects dans une analyse.

J'ai donc tenté de rassembler mes _îles_ dans 6 micro-archipels, `1, 2, 3, 4, 5, 6`, puis sur chaque île placer mes _anecdotes_ et de faire les liens entre les micro-archipels avec les _théories_ pour composer un Archipel qui se lie / lit et se forme par les justifications de ces théories.

**Recherche-action** : est une itération permanente entre la « recherche-intervention » et la « recherche-expérimentation ». Une démarche de recherche qualitative visant à résoudre un problème concret tout en contribuant à la production de connaissances scientifiques grâce à l’action volontaire d’une ou plusieurs personnes avec un protocole. 

**Théorie incarnée** : Selon la théorie de la cognition incarnée, nos capacités cognitives seraient influencées par nos expériences sensori-motrices.

> « _Il fait référence aux pensées (cognition), aux sentiments (émotion) et aux comportements (corps) basés sur nos expériences sensorielles et sur nos positions corporelles. Dans la pratique, il est utilisé pour penser des aspects généralement associés à notre vie quotidienne, tels que notre façon de bouger, de parler et de se développer » [Wikipedia](https://fr.wikipedia.org/wiki/Embodiment)

Jusqu'ici il y a peu de différence entre ce que je tente de composer et la forme « classique » de la _conférence gesticulée_, bien qu'elle puisse effectivement prendre tout un tas de formes diverses et variées selon les écrits des dépositaires de cette forme. 

Je n'ai à cette étape pas encore rédigé de _textes_ de conférences. J'ai composé un base de scénario.

### S'inspirer Dehors

> « _Soyez toujours pour vous-mêmes votre dehors, le dehors de toute chose._ » Alain Damasio, La Zone du Dehors

Je cherche donc une grande implication du public lors de cette anticonférence gesticulée, participation active. J'ai déjà fait _la personne intéressante_ sur scène pour des conférences au format _magistral_, j'ai tenté des conférences où j'essaie de me fondre dans le public. J'assume d'avoir proposé de transmettre des choses lors de ce festival dans un format « conférence ». Là j'ai l'envie profonde et je suis convaincu de tenter un public _intéressé et intéressant_ dans le lequel je ne suis plus qu'une composante parmi les autres. Je suis convaincu que c'est ensemble que nous trouverons des chemins de libertés sur les sujets que nous allons attaquer.

J'ai alors cherché inspirations et méthodes sur des formats différents de la _conférence gesticulée_.

Par exemple :

+ Magic Meeting,  des spectacles de rue participatifs tout public inédits en déambulation utilisant la technologie des casques UHF. <https://www.magic-meeting.com>
+ Se réapproprier l'espace public et atelier de Rue par le collectif Chiendent <https://www.multibao.org/#chiendent/atelier-de-rue>
+ Atelier pudeur et chiffrement <https://hackingwithcare.in/2018/05/atelier-pudeur-et-chiffrement>
+ Hacking with care <https://hackingwithcare.in/2016/08/hacking-with-care-sessions-34-sept-berlin-a-schedule> & <https://hackingwithcare.in/wiki/doku.php>

Ou encore un logiciel :
+ Twine is an open-source tool for telling interactive, nonlinear stories. <https://twinery.org>

J'ai aussi décidé de faire un ou deux moments avec uniquement des isogrammes, c'est à dire avec des mots qui n'ont pas de lettres répétitives, consécutives ou non consécutives. Ceci pouvant me servir pour illustrer des principes d'énigmes lorsqu'il sagit de déchiffrer ou de protéger _une chose à cacher_. 

Et un simple jeu de cartons que je prépare à l'avance sur lesquels sont inscrites toutes les îles choisies auparavant qui seront à disposition du public à tout moment pour qu'elle ou il puisse dire « j'ai envie que l'on parle de ça maintenant car... » ou simplement designer une île par le geste. 

Il me reste pas mal de _choses_ à tester pour cette dernière phase de conception avant écriture des _textes_. 

J'ai choisi un bar pour vivre ce moment. Je vais donc tenter de ne pas dénaturer ce que le bar est : un lieu de rencontres, de discussions, de mouvements, de plaisirs, de musique.

### Répéter et se faire aider

_Tester sa trame et son expérience_

C'est donc dans ce bar que j'irais répéter des mouvements de vie plus que des textes à réciter. Deux fois au moins. Cela me semble important pour :
+ Maîtriser l'espace
+ Conforter la relation avec le proprio
+ Être à son aise le jour J

Cela ne pourrait être suffisant pour mettre toute les chances de son côté pour un _bon moment_ gesticulé. J'ai donc aussi contacté des conférencières et conférenciers qui gesticulent pour leur demander avis, critiques, conseils ; des artites du théâtre. Et surtout quelques ami⋅e⋅s pour affiner et distiller le contenu et éprouver la forme jusqu'à obtenir l'Ivraie tant escomptée.

C'est bien là le plus important, faire ensemble dès le début, écrire, tester, retourner, documenter, partager, librement pour que chacune et chacun puisse en faire apprentissage par son propre usage, sa culture populaire. Une culture libre qui est une résistance. 

_Image d'en_tête, Fixing a Brain By j4p4n, [Universal (CC0 1.0)Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/)_

## Ref de recherces

1. La grand parquet la Scop le Pavé via rise-up.net <https://we.riseup.net/assets/34812/conseils+pour+une+conf%C3%A9rence+gesticul%C3%A9e.doc>

2. Vidéo de conférence gesticulée « Informatique ou libertés » de Lunar <https://peertube.tamanoir.foucry.net/videos/watch/1ffc6a9a-20f8-4cb0-8753-35a88c69cc9e>

3. Idéologie postmoderne :
- <http://www.marcel-kuntz-ogm.fr/2015/03/volonte-mainmise-sur-science.html>
- « Sciences citoyennes » : une version postmoderne de la « science prolétarienne » s’introduit au CNRS <https://www.pseudo-sciences.org/spip.php?article2081>
- Peurs ou intérêt général : les dangers de la « démocratie participative » <https://www.pseudo-sciences.org/spip.php?article2129>

4. playlist post punk celte sur Internet Archive <https://archive.org/search.php?query=breizh&and[]=subject%3A%22post+punk%22>
5. Playlist de Valere sur peertube <https://peertube.tamanoir.foucry.net/video-channels/fded4155-fb1a-4b18-993b-e8f819772b07@peer.hostux.social/videos>
6. Le rôle du corps dans les apprentissages symboliques : apports des théories de la cognition incarnée et de la charge cognitive, Florence Bara, André Tricot, <https://hal.archives-ouvertes.fr/hal-01628840/document>

## Notes de page

[^1]: la page wikipedia de Franck Lepage <https://fr.wikipedia.org/wiki/Franck_Lepage>

[^2]: video de conférence gesticulée de Franck Lepage, Inculture(s) 1 : La culture  https://www.youtube.com/watch?v=9MCU7ALAq0Q ; et Inculture(s) 2 : L'éducation : https://www.youtube.com/watch?v=ACxRSSkYR_k

[^3]: le livre numérique concernant les 18 mois de l'exprience fork the world, sous licence libre, donne à voir, comprendre et s'approprier notre démarche axée autour des tiers-lieux libres et open source <https://world-trust-foundation.gitbook.io/fork-the-world/>

[^4]: article openEdition par Alice Krieg-Planque, Université Paris-Est Créteil, issu de «Intimie et poltique », 2012, page 165-168, <https://journals.openedition.org/itineraires/1206>

[^5]: « Politiser sa trajectoire, démocratiser les savoirs », La fabrique des « conférenciers gesticulants », Nicolas Brusadelli,Agora débats/jeunesses 2017/2 (N° 76), pages 93 à 106, <https://www.cairn.info/revue-agora-debats-jeunesses-2017-2-page-93.htm>, compre rendu par Pierre Marie sur OpenEdition <https://journals.openedition.org/lectures/23692>

[^6]: voir la conférence de Ludovic Arnorld, « Le tiers-lieu, le numérique et le PMU » <https://www.youtube.com/watch?v=kFsDL_e2KMM>

[^7]: J'essaie de dénouer cette schyzophrénie avec par exemple la science fiction et les réflexion d'Alain Damasio <https://xavcc.frama.io/damasio-rennes>
