---
title: " Recherche-action Biononymous et Bioprivacy "
layout: post
date: 2018-03-20 22:10
tag:
- Biononymous
- Bioprivacy
- Biomimétisme
- Libertés
- Gouvernance
image: /assets/images/human-dna.png
headerImage: true
projects: true
hidden: true # don't count this post in blog pagination
description: concernant la surveillance morphologique - biologique, plus largement l'intimité et confidentialité du vivant
category: project
author: XavierCoadic
externalLink: false
---

Travaux de recherches et actions menées

#### Table des contenus

+ [Publications](#publication)
  + [Blog](#blog)
  + [Revues](#revues)
  + [Curations et Informations](#curation-et-informations)
+ [Ateliers](#ateliers)
  + [2019](#2019)
  + [2018](#2018)
  + [2017](#2017)
  + [Autres](#autres)
+ [Interventions](#interventions)
  + [Festival des libertés numériques 2019 à Rennes](#fdln-2019-rennes)
  + [Web2Day 2018 Nantes](#web2day-2018-nantes)

## Publications

### Blog

+ [Conférence au web2day 2018 : biononymous et bioprivacy](/biononymous-web2day), juin 2018
+ [Petite histoire d'introduction à Biononymous et Bioprivacy](/biononymous), mars 2018
+ [Vous êtes biologiquement piratable par un enfant de 12 ans](/bioware), novembre 2017

### Revues

### Curation et informations

+ [Biononymous et Bioprivacy - 6](https://framastory.org/story/xavcc/biononymous-et-bioprivacy-6)
+ [Biononymous et Bioprivacy - 5](https://framastory.org/story/xavcc/biononymous-et-bioprivacy-5)
+ [Biononymous et Bioprivacy - 4](https://framastory.org/story/xavcc/biononymous-et-bioprivacy-4)
+ [Biononymous et Bioprivacy - 3](https://framastory.org/story/xavcc/biononymous-et-bioprivacy-3/)
+ [Biononymous et Bioprivacy - 2](https://framastory.org/story/xavcc/biononymous-et-bioprivacy-2/)
+ [Biononymous et Bioprivacy - 1](https://framastory.org/story/xavcc/biononymous-et-bioprivacy-1/)

## Ateliers

### 2019
+ [Atelier « Biopanique et auto-défense bio-numérique »](https://fdln.insa-rennes.fr/agir/atelier-biopanique-et-auto-defense-bio-numerique/)
+ [Atelier reconnaissance faciale et morphologique de masse](https://fdln.insa-rennes.fr/agir/ateliers-protegez-votre-vie-privee-et-passez-au-libre)

### 2018
+ [Atelier d’initiation Biononymous & bioprivacy](https://xavcc.github.io/formations/wiki/biononymous-octobre-2018.html), octobre 2018, Rennes
+ [Atelier ADN IndieCamp](https://movilab.org/index.php?title=IndieCamp_Kerbors_2018), Août 2018, Kerbos
+ [Forum des usages coopératifs](https://forum-usages-cooperatifs.net/index.php/Tiers_lieux,_le_libre_et_l%27open_source_et_la_collaboration_pour_r%C3%A9pondre_aux_enjeux_critiques), Juillet 2018, Brest
+ [Parcours numérique](https://twitter.com/assoPiNG/status/979292011076481024), mars 2018, Nantes

### 2017
+ [BioPanique, Cuisine et Féminisme](https://www.unidivers.fr/rennes/biopanique-cuisine-et-feminisme/), 7 octobre 2017, Rennes

### Autres

+ travaux avec [Le Biome HackLab](https://lebiome.github.io)
+ Autres Trucs
  + [levures](https://wiki.breizh-entropy.org/wiki/Levures_DIY)
  + [Incabuteur lowtech](https://wiki.breizh-entropy.org/wiki/Incubateur_lowtech)
  + [bioimprimante 3D](https://wiki.breizh-entropy.org/wiki/Bioimprimante_3d)

## Interventions

### Fdln 2019 Rennes

+ [Conférence « Biononymous Bioprivacy »](https://fdln.insa-rennes.fr/decrypter/conference-biononymous-bioprivacy/)

### Web2Day 2018 Nantes

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts" src="https://peertube.tamanoir.foucry.net/videos/embed/1bbcbfdd-b463-4d33-8dca-02e5adfc245c" frameborder="0" allowfullscreen></iframe>