---
title: "Homo Spectaculus : échographie et regard sur la marchandisation du poop numérique"
layout: post
date: 2018-09-18 16:44
image: /assets/images/1962-L-04111.jpg
headerImage: true
tag:
- hsociety
- Nantes
- Tiers-Lieux
star: false
category: blog
author: XavierCoadic
description: Qu'allons nous faire aux communs numériques francophones ?
---

Du petit matin au café à l'oreille dans les radios, dans les discours politiques, dans le pitch des start-ups, jusqu'aux rassemblements conséquents et corporatistes d'individus, j'entends et lis à profusion et orgie qu'il faut « remetre l'humain au centre » et que beaucoup développent des systèmes, méthodes, technologies « centrées sur l'humain ». Ne vous aventurez pas trop à poser la question « Qu'est-ce que l'humain ? », vous pourriez vous attirer du déni, voire le dédain, de l'orateur ou de l'oratrice qui crie promesse. 

Mais alors, puisque de réponse nous sommes privé⋅e⋅s et de discours inondé⋅e⋅s, quel panorama pourrait être masqué par ce phénomène acoustique de réflexion d'un son répété ? Quelles structures sont fissurées par ce phénomène accoustique et jeu de langage ?

`Édité le 21 septembre 2018`

## Homo spectaculus

J'ai récemment pris l'occasion de participer activement à ce qui fut un jour nommé « les assises de médiation numérique », aujourd'hui réintitulées « Numérique en Commun ». Nous y prenions part comme co-concepteurs, co-conceptrices et co-artisans d'un  MediaLab, dont la [page de docuemntation sur un wiki est WIP](http://movilab.org/index.php?title=Medialab_NEC_2018) (Work In Progess ou Work in Promise selon votre approche critique).

Au cœur de cet événement, je n'ai pas échappé à la mise en scène et acte de spectacle qui s'y jouaient, parfois même en qualité de contributeur, bien que nous cherchions à proposer une autre forme d'agentivité. Dans cet espace-temps, comme dans beaucoup d'autres, les injonctions d'inclusion, de co-quelque chose, les mots clés et les promesses, tombaient des rideaux sans pour autant être expressives. 

Cet article souffre lui aussi de travers de tournures pas toujours accessibles, d'emploi de mots avec manque de formalisme simple, d'une présentation abrupte. L'expressivité doit être lue et comprise à la fois comme la puissance d'expression, la capacité à exprimer une opération complexe par un formalisme aussi simple que possible, et comme la qualité de l'expression, le fait que le texte du formalisme suggère facilement sa signification à l'auditeur. Cela ne semble être vrai que pour programme simple, faisant peu de choses. Dès que le programme est non-trivial, il faut apprendre, donc que quelqu'un explique.
Ainsi, encore une fois là où sont exposés et promis d'être traités des enjeux de société, des enjeux humains, ont peut avoir le sentiment d'un spectacle assourdissant, d'une représentation noyée dans des bruits. 

Nous vivons dans une société du spectacle et cette hypothèse n'est pas nouvelle. Guy Debord écrivait sur ces enjuex dès 1967. Dans leurs travaux sur la fabrication du consentement et les modèles de propagande, Noam Choamsky et Edward Herman agrémentaient cette société du spectacle avec un modèle basé sur cinq filtres qui déterminent en grande partie l'information produite dans et par les médias, à savoir : 
+ les caractéristiques économiques du média considéré (taille, actionnariat, orientation lucrative), 
+ la régulation par la publicité, 
+ la nature des sources d'information employées, 
+ les « contre-feux » (« flak ») et moyens de pression, 
+ l'idéologie anti contre-pouvoir

Cela peut être entendu et étendu en 10 axes stratégiques que l'on retrouve dans la mise en scène du spectacle et dans les discours qui habillent le spectaculaire visant à manipuler et contrôler une foule :
+ La stratégie de la distraction
+ Créer des problèmes, puis offrir des solutions
+ La stratégie de la dégradation, pour faire accepter une mesure inacceptable
+ La stratégie du différé, façon de faire accepter une décision impopulaire et de la présenter comme « douloureuse mais nécessaire »
+ S’adresser au public comme à des enfants en bas-âge
+ Faire appel à l’émotionnel plutôt qu’à la réflexion
+ Maintenir le public dans l’ignorance et la bêtise
+ Encourager le public à se complaire dans la médiocrité, promouvoir le « cool » jusqu'à une tyrannie du  « fun »
+ Remplacer la révolte par la culpabilité
+ Connaître les individus mieux qu’ils ne se connaissent eux-mêmes

Au regard de la configuration actuelle de notre société occidentale se désirant hyper-connectée cela prend une toute autre ampleur. Ce spectacle semble s'exprimer dans une scoiété basée sur une économie de l'affect, un capitalisme de l'émotion.

> « _Depuis le développement d’un « Internet commercial » les organisations cherchent à quantifier les interactions et discours des internautes afin d’y identifier des publics et favoriser le placement publicitaire. Mais peut-on « mesurer » les publics numériques ? Ces mesures ne fabriquent-elles pas l’objet « public » plutôt que de le révéler ? Tout comme ces « métriques » construisent de la (e)réputation là où il n’y en peut-être pas…_ » [Camille Alloing](https://cadderep.hypotheses.org/category/recherches-et-reflexions)

L'un des buts principaux de ce capitalisme de l'affect est de renforcer la surveillance des foules et de l'individu. L'exemple non anecdotique de l'établissement de fiches et classements indivuels par DisInfo Lab lors de la réaction, souvent émotionnelle, d'internautes sur twitter lors de « l'affaire Banalla » en est relent récent (source: [Internet Atcu](https://www.nextinpact.com/news/107027-affaire-benalla-et-disinfo-lab-250-plaintes-recues-par-cnil.htm)).

Mais alors dans un société basée sur le spectacle, pourquoi un petit groupe de personnes (300 à 400 ici à Nantes) se réunit autour du thème de la médiation numérique et des communs, puis quelles questions adressent-elles ? (voir également « [Quelques questions d'actualités concommitantes à Numérique en Commun](http://movilab.org/index.php?title=Medialab_NEC_2018#Questions_d.27actualit.C3.A9s_concomitantes_.C3.A0_NEC_2018)»)

Elles et ils sont venues écouter des sachant⋅e⋅s, elles et ils sont venu⋅e⋅s vendre et/ou vanter leurs scénari, elles et ils sont venu⋅e⋅s troquer des promesses. Une place de marché sur laquelle se (re)joue une divine comédie entre « spécialistes », oubliant parfois d'interroger leur rapport à la culture numérique comme culture technique, oubliant parfois que ce qui se joue est politique puisqu'il s'agit de transformer la cité. 

En 2018, la société du spectacle passe par l'hyper-individuation, le poids économiques des « followers », la force économique et financière de celle ou de celui qui tient le micro. Capitalisme de surveillance, économie de l'émotion et hyper-individuation forment alors un Ouroboros accentuant les écarts entre la personne qui est instituée et les personnes fracturées. Ceci a une valeur marchande. Les grands ou moins grands forums, quelle que soit la thématique, articulés ainsi ne sont que des reflets de cette société du spectacle. 

## Langage, grammaire et endettement

Si nous avons pris espace et temps au sein de ce Numérique en commun pour tenter d'y faire un MediaLab c'est notamment grâce aux travaux et efforts de qualité de l'[association Ping](https://info.pingbase.net). Nous avons tenté de concevoir et mettre en œuvre un processus que nous n'avons pas inventé, peut être adpaté ou remixé.

>  « _[…]ces dynamiques se sont cristallisées d’une manière tout particulièrement visible à partir de 1995 quand, se créent, à la faveur de l’organisation des contre-sommets altermondialistes, les premiers « média-lab » qui connaîtront une période de grande activité durant une dizaine d’années, mais aussi le réseau libertaire Indymedia dont le mot d’ordre « don’t hate the media, be the media » résonne fortement avec la conviction du hackerisme libertaire selon laquelle le net est le lieu d’une potentielle émancipation des individus dans un esprit d’horizontalité et d’égalité._ » [Numérique et émancipation de la politique du code au renouvellement des élites, Nicolas Auray](http://mouvements.info/numerique-et-emancipation-de-la-politique-du-code-au-renouvellement-des-elites/)

Nous pouvions également nous appuyer, parmi tant d'autres supports, médium et ressources, sur l'expérience de Julien Bellanger qu'il exprime avec maitrise, compréhension et critiques dans « [Lieux numériques, entre pratiques populaires et ré- appropriation des technologies ?](https://medium.com/@julbel/lieux-trans-num%C3%A9riques-entre-pratiques-populaires-et-r%C3%A9-appropriation-des-technologies-ec375c90532b) ».

Pour jauger et juger des conséquences, délétères ou bénéfiques, de ce MediaLab il faudra, pour nous comme pour toute personne observatrice extérieure, déployer encore un peu de temps et d'énergie pour décortiquer et analyser, puis interpréter les effets. Mais ce temps viendra.

Cependant, cette place « de l'intérieur » n'enlève pas la capacité d'observation et d'interrogation sur le phénomène de mise en spectacle et sur d'autres aspects. L'événement numérique en commun n'étant ici qu'un objet révélateur récent, tout comme pourraient être tant d'autres configurations similaires. L'humain devait être partout au centre, du moins dans les discours, la médiation, l'inclusion et réduction de fracture étaient des urgences, le partage et la culture du Co- étaient les arguments de « bienveillance ». D'ailleurs, si vous observez des configurations comparables mais traitant d'écologie vous trouverez certainement des similitudes et des mécanismes. 

Pour négocier dans ces discours, ne serait-ce que le champs lexical dans lequel l'hypothèse d'une culture commune existerait, voire au mieux l'entretien d'un commun, il nous faut nous endetter grammaticalement face à des individus en posture de commandement, nous devons souscrire un emprunt par le langage qui permettrait de financer des investissements conséquents, ou juste de survivalisme, tout en préservant le siège de pouvoir du créditeur. C'est à dire que nous cédons, par exemple, à l'arrivée conquérante de termes provenant du « langage » attribués à la culture start-up, quand peut être une telle culture existerait, dans les sphères de la médiation numérique, des communautés des communs, des bibliothèques et autres tiers-lieux. C'est une dualité entre l'usage d'un langage et les conditions d'acceptabilité qui vont avec ce langage. 

En cela, nous ne semblons pas changer ni la configuration, celle du spectacle et d'une pyramide hiérarchique, ni les rêgles de ce spectacle, c'est à dire qui scénarise, qui joue les premiers rôles et qui font figurants, qui encaissent les recettes et qui et comment redistribuent les dividendes. Ou peut être que nous avons trop changé les choses pour espérer autre gabelle que des promesses. Ce qui est attendu comme un travail en cours avec urgence (WIP pour Work in Progess) devient une promesse en cours (Work In Promise), et les subtilités de la FUNification entre en scène aussi par le langage. 

Lors de la rétrospective de conclusion [Alain Giffard](http://bbf.enssib.fr/biographie/bbf-2011-05-0071-013/Giffard/Alain) proposa d’interroger notre langage commun. Ce langage qui a fonctionné comme un dialecte de communication entre d’un côté les hacktivistes (de la médiation et de l’inclusion) et les politiques. Il a fonctionné comme un langage spécialisé, une novlangue un peu particulière.

Certains ont dû avoir un sentiment de hors-sol d'après Alain Giffard… Mais ce langage est important pour qu’il se passe quelque chose entre acteurs de la médiation et politiques. Nous échangeons du vocabulaire et de la grammaire. Quel est alors le prix à payer pour utiliser ce langage ? Quelle est la contre-dette ? En quoi nous permet-il (ou non) de mémoriser, critiquer, partager notre expérience collective ? Quelle relation entre expérience et ce langage ?

> « _Se poser la question de notre langage commun par rapport à notre expérience c’est poser la question du rapport à la culture numérique comme culture technique. Nous ne pouvons pas ne pas construire une telle culture numérique comme une culture technique, et donc nous ne pouvons pas ne pas interroger les langages. On s’attend à ce que cette culture numérique soit critique. Il faut que cette culture technique se (re)constitue comme une culture critique. Elle ne doit pas être confinée à un régime des objets, des choses, des données, des techniques, fussent-elles un commun. La culture numérique n’est pas un moyen pour maîtriser la technique. Nous pouvons maîtriser la relation à la technique, avoir une relation significative. C’est ce que nous avons fait au cours de ces deux jours._ » Alain Giffard

## Culture poop remixée pour un poopulisme numérique ?

Des micro-groupes qui exercent l'acte d'injonction aux usages numériques ou à l'inclusion. Un mouvement né sur un malentendu ? Mais qui semble perdurer et prendre de l'ampleur grâce à l'établissement d'une « (micro)communauté » engagée avec des règles basées sur la théâtralité de la peopolisation (angliscisme de novlang pour dire hyper-individuation). Ces protocoles régis par la récurrence d'effets de style et la volonté de mettre les sens des spectateurs à l'épreuve de la répétition rétinienne et auditive d'éléments de langage dans un flux permanent de promesses aigües semblent constituer les traits caractéristiques du spectacle du mainstream (angliscisme en novlang pour ordre établi) numérique à la française actuel. De l'usage de boucles de bégaiement (stutter loop) dans le discours pour mettre en avant le fait que tel acteur effectue  « un pivot » pour (re-stutter loop) « placer l'humain au centre », pousser la répétition cent fois pour hypnotiser son auditorat. Pourrait-on aussi parler de viol de l'oreille (ear rape) lorsque surgissent de nouvelles expressions « punch line ». Alors, peut enfin surgir le mélange de phrases (sentence mixing) pour reformer des mots, dès lors faire de l'apparition d'une nouvelle discipline ancrée dans la disruption.  

> _Poop: mot anglais, caca. Poop culture : absurdist remixes that ape and mock the lowest technical and aesthetic standards of remix culture to comment on remix culture. des remixes absurdes qui se moquent des standards techniques et esthétiques les plus bas de la culture remix pour commenter la culture remix._

Ces mécanismes cherchent ouvertement à bousculer les règles précédemment établies par d'anciennes et d'anciens défricheurs et défricheuses. Des petits pas de dance pour remplacer des prophètes par d'autres prophètes, tout ceci subtilement construit sur une forme de [situationisme](https://fr.wikipedia.org/wiki/Situationisme) détournée et renversée au service du spectacle d'un pouvoir déjà installé. 

La culture du Remix faisait des émules il y encore quelques années (Afrika Bambaataa, RIP Manifesto, Muséomix...) mais semblent aujourd'hui repris à d'autres desseins ; les hackathons devenant pour beaucoup des courses à la concurrence d'exploitation par le travail gratuit, on peut se poser les questions de l'existence présente d'un rejet botanique né de différents croisements qui prendrait la forme d'un poopulisme numérique. 

Celles et ceux qui se draperaient d'apparence punk-décripteur-**capitaliste** avec une dimension participative pour inclure ne font que le jeu spectaculaire d'un poopulisme dans lequel une masse fournit d'un côté les efforts qui nourrissent la position de pouvoir établie par le petit groupe sachant⋅e⋅s auto-promu⋅e⋅s, et de l'autre côté cette masse fournit l'alibi du maintien de cette position dominante puisqu'il faut bien « éduquer » et « inclure » quelque part cette masse. Cela serait ressembler, à plus petite échelle et dans le même cheminement, à l'histoire de Startbucks Coffee depuis le Seattle de la contre culture au « total washing » actuel.

> « _C'est pas un hub qui fédère les acteurs de l'inclusion numérique qui va aider les exclus du numérique à devenir autonomes_» [Thomas Fourneux](http://biblionumericus.fr)

Du regard critique sur l'écho des mots usés, des dessins que l'on pourrait tirer, avec ou sans satire, des grands raouts dans lesquels un néo-vocable est asséné pour convaincre par la répétition avec les rebonds de cette répétition sur les murs de tour d'ivoire, on peut se laisser convaincre d'une silouhette de contestation dans ce mouvement, mais cela n'en possède que l'ombre et n'en a ni le goût, ni l'odeur, ni l'âme. Le youtube poop des années 2010 critiquait la société du spectacle, le prêt à consommer, le prêt à l'emploi. le Poopulisme numérique naissant remix la culture du Remix elle-même, et retourne ces contestations pour entretenir dans une novlangue le spectacle en tant que rapport social, pour réaffirmer les représentations immuables, autrement écrit cela ne sert qu'à nourrir une économie de conservation des pouvoirs. Ce qui est nouveau aujourd'hui tient dans l'extrème horreur que cette société du spectacle et d'économie des émotions est instituée pour renforcer les mécanismes de surveillance de masse et de contrôle individuel. L'écho des mots des promesses n'étant qu'un stratagème des orateurs. Un écho dans lequel des communs traités comme des biens privés sont fissurés pour être revendus morceaux par morceaux.

> « [...] _on attend toujours un signe tangible de l’actuel gouvernement dont les premières réalisations législatives en matière de numérique ont paru plus enclines à renforcer la sécurité et la propriété qu’à aller dans le sens des Communs._ » Collectif SavoirCom1 "[Numérique en Commun(s) : Pour une ouverture des Communs sans récupération](http://www.savoirscom1.info/2018/09/numerique-en-communs-pour-une-ouverture-des-communs-sans-recuperation/?utm_source=feedburner&utm_medium=twitter&utm_campaign=Feed%3A+Savoirscom1+%28SavoirsCom1%29)", 19 spetembre 2018

On peut alors envisager de s'accorder avec la trajectoire de [Lawrence Lessig](https://fr.wikipedia.org/wiki/Lawrence_Lessig), qui est passé de l'étude de la propriété intellectuelle, à l'écriture sur la culture du remix pour aujourd'hui être un chercheur et défenseur de la démocratie, comme un symbole d'avertissement à notre propre glissement vers de graves problèmes.

Cette orientation aux risques de poopnumérique tend vers une marchandisation centralisée de chaque composant, depuis les discours, jusqu'à la médiation en passant par les œuvres, les supports, les réseaux. Ce ne sera pas sans conséquences pour l'ensemble des populations.

Si nous n'arrivons pas à (re)faire collectif, (re)instituer une grammaire, (ré)établir des codes, (re)détourner, (re)ouvrir ce qui est fermé, pour réifier la culture libre et décentralisée d'Internet et du numérique, alors il nous faudra faire le deuil de ce qui encore aujourd'hui peut être l'hyptothèse d'un commun. Il ne se présente ici pas tant de critiquer un processus d'institutionnalisation mais de mettre en fond et forme des propositions de valeurs et des actions. Des actes qui ne prennent pas le chemin d'une ou des institutions qui, elles, ne préservent que leur établissement de posture pour profiter de la marchandisation de ce qui fut autrefois partie de la chose publique et qui pourrait être, ou redevenir, aujourd'hui un commun.

> « _Some obses over making "good" poops fuck that shit_ » Apocryphal statement, Internet.

Mais là encore plane le spectre d'un Work In Promise. Cependant, nous n'avons jamais usé de la culture du libre, des ses œuvres, des logiciels, des licences, dans le but d'enfermer création ou remix de création car il serait tout aussi dangereux d'instituer le dogme d'une culture ou d'imposer les canons de la réutilisation que de laisser prospérer la marchandisation dévorante. Il est tout simplement aujourd'hui important et urgent de ne pas sombrer dans la marchandisation vorace des commmuns, des cultures, de la médiation numérique, des tiers-lieux. Ce qui ne signifie en aucun cas laisser les personnes composants « les publics en fragilités », ni les acteurs et les actrices de ces hacktivités dans une précarité de reconnaissance, ou technique, ou économique. C'est déjà là un premier pas de traitement et de considération adéquat.

Et si nous commencions par définir ce qu'est l'humain avant de l'invoquer ésotériquement ? Si nous sommes bien Homo Sapiens, nous devrions peut être avec un peu de sérieux trouver matière à regarder nos processus d'hominisation et d'essayer d'en tirer ce qui fait de nous des humains, Homo Hominis, puis traiter la promesse d'humanité, avant de nous confondre en Homo numéricus ou Homo spectaculus. Comme si devenir humain était une quête de liberté et d'émmancipation, ce qui n'est peut être pas si unique : des idéaux qui ne sont peut être toujours pas atteints aujourd'hui.

## Contributions et remerciements

Ces personnes sont co-autrices de ce texte et sont ainsi inclues dans les conditions de la licence attribuée.

+ [Maïa Dereva](http://contributions.maiadereva.net/), P2P foudation et Communs francophone, pour ses contribtions au MediaLab, à ce texte et aux réflexions toujours en cours.
