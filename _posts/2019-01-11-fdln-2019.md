---
title: Festival des libertés numériques 2019, mon plan de préparation et ma recherche de materia blanche
layout: post
date: 2019-01-11 17:30
image: /assets/images/Light-in-the-darkness.jpg
headerImage: true
tag:
- Numérique
- Rennes
- Open Source
- hsociety
category: blog
author: XavierCoadic
description: Introduction avant préparation, documentation, retour d'expériences et publications.
---

En janvier et février 2019 se déroulera dans plusieurs villes sur un large grand ouest en France le [festival des libertés numériques](https://fdln.insa-rennes.fr/le-festival) (et aussi à Paris), du 25 janvier au 9 février.

> « _Surveillances, « grandes oreilles », dystopies…Depuis plusieurs années, l’environnement dans lequel nous baignons est teinté de scandales, de fuites de données, d’imaginaires horrifiants et pessimistes.
Cette 2ème édition du Festival des Libertés Numériques nous invite à plonger ensemble dans le grand bain pour partir à la recherche d’un trésor : notre pouvoir d’agir collectivement pour la préservation de nos libertés et de nos vies privées_ ».

Amorcé et coordonné par les bibliothécaires de l’INSA Rennes, c'est l'occasion de rencontres, d'ateliers, de discussions, de conférences, d'échanges, sur de nombreux problèmes, défis et solutions qui nous concernent toutes et tous : les libertés, nos intimités, nos droits, nos vies privées. 

J'en profiterais pour me rendre à plusieurs évènements, comme par exemple la conférence de Léna Dormeau[^1], le 1er fèvrier dans la catégorie « Transplaner », sur les NBIC : 

 > « _Interroger l’une des facettes du tournant numérique que constitue la révolution dites « NBIC ». Cette dernière, qui opère une découpe de l’être humain en 4 parts_ : **N**anotechnologies, **B**iotechnologies, **I**nformatique et **C**ognitif, _semble vouloir s’approcher plus encore de la quête d’une humanité augmentée, d’une possible immortalité. La question du corps, et des corps, devenant des objets pensés par des prismes techniques, constituera notre objet de réflexion principale_ ».
 
 J'interviendrais également à trois reprises lors de ce festival sur un sujet au titre déroutant ou intriguant :
 
 1. [Anti-Conférence « Biononymous Bioprivacy »](https://fdln.insa-rennes.fr/decrypter/conference-biononymous-bioprivacy/), le 4 février à 19h, bd de la liberté à Rennes, prix libre. Introduction aux concept et enjeux de biononymous & bioprivacy.
 2. [Atelier « Biopanique et auto-défense bio-numérique »](https://fdln.insa-rennes.fr/agir/atelier-biopanique-et-auto-defense-bio-numerique/), 5 février, 19h, Rennes, prix libre.
 3. [Atelier reconnaissance faciale et morphologique de masse](https://fdln.insa-rennes.fr/agir/ateliers-protegez-votre-vie-privee-et-passez-au-libre/), 7 février, 18h, INSA Rennes, gratuit.
 
Pour concevoir et réaliser ces trois moments j'utilise des vécus, des connaissances et des savoirs, acquis depuis plusieurs années. Mais je dois aussi travailler sérieusement pour viser une qualité appréciable et fournir la matière à chaque personne permettant d'apprendre ensemble. Je dois donc apprendre de nouvelles connaissances, compténces, connaissances et savoir-faire.

C'est tout cela que je souhaite mettre à disposition au plus grand nombre sur l'internet, sous licence libre comme à l'habitude, en plus de ces trois moments en présentiel à Rennes. La documentation libre me tient à cœur, je considère qu'elle permet par certaines pratiques des moyens ou des techniques de résistances (je prépare une série d'articles pour la revue [Sens Public](https://www.sens-public.org) à ce sujet).

Je vais donc utiliser mon blog pour publier les documentations, tutoriels, retours d'expériences (et peut être plus), les sources, notes de recherches, qui me permettent de concevoir et de réaliser ces actions. Je commencerais dès aujourd'hui par vous écrire mon plan de prépration et vous expliquer ma recherche de materia blanche[^2].

## Plan de préparation et documentations

Le festival des libertés est numériques fonctionne ainsi : une diffusion d'un appel à proposition à lieu plusieurs mois avant le début du festival, vous êtes libres d'y répondre en choisissant des modalités d'accès (payant, prix libre, gratuité), si votre suggestion est retenue vous êtes contact⋅é⋅e⋅s pour en discuter, si les parties sont d'accord vous figurez au programme. Ensuite à vous d'agir pour trouver les moyens de réaliser votre propositions (lieu, matériel, éventuel aide financière), l'équipe de la bibliothèque de l'INSA Rennes pouvant vous aiguiller, autant que faire se peut, et vous fournira également des supports de communication. Vous êtes volontaire et bénévole de votre proposition.

J'ai ainsi répondu il y a déjà un moment à l'appel à propostion et je suis informé de ma responsabilité d'intervenant depuis plusieurs semaines. J'aurais certainement besoin de vous lectrice, lecteur, j'y réviendrais au fur et à mesure de ce billet blog.

Voici donc le plan des contenus et travaux que je prévois de mettre librement à disposition : 

1. Ce plan de préparation ci-publié
2. [Apprendre, préparer, répéter, réaliser, un anti-conférence ou getsiculée](/fdln-anticonference-gesticulee)
3. [Introduction à l'auto-défense bionumérique](/fdln-bioprivacy)
  + Extraire de l'ADN soi-même ou à plusieurs avec des produits courants
  + Brouiller, obfusquer, effacer, des traces génétiques
  + Les questions de perte d'intimité biologique
  + Le Vivant comme un commun ou le Vivant confisqué
  + Le contrôle social par privatisation du Vivant
4. Introduction à la reconnaissance morphologique
  + **Concevoir en direct avec le public un jeu grand public de _faceblind_ en coopérant sur un langage de programmation** (quelque soit votre niveau)
  + Scénario pédagogique
  + Comprendre les enjeux en pratiquant
  + Apprendre en pair à pair
  + Code et explication
  + **Concevoir en direct avec le public un module de reconnaissance faciale en coopération** (tous niveaux)
  + Scénario pédagogique
  + Comprendre les enjeux en pratiquant
  + Apprendre en pair à pair
  + Se protéger et déjouer la surveillance
  + Code et explication
  + **Digital labor, contrôle social, surveillance généralisée**
5. Retours d'expériences

J'ai un peu de pain sur la planche... 

Ainsi, depuis tout début décembre j'ai commencé à bûcher sur « l'anti-conférence » que je dois réaliser.

### Anti-conférence ou conférence gesticulée ?

Biononymous et bioprivacy[^3] est le sujet que j'ai proposé de traiter durant ce festival. L'anglicisme de l'appelation, les enjeux profonds et complexes peuvent apporter des problèmes d'appréhension et de compréhension, j'ai donc opté pour une séance d'introduction. Je ne souhaite pas un temps magistral durant cette présentation, je n'apprécie que peu l'information descendante en sens unique et encore moins de placer le public en posture de subir et de passivité. Lors du mois de mai 2018 j'avais tenté une forme un peu différente de conférence[^4].

Cette fois ci je souhaitais travailler sur une conférence gesticulée[^5], un acte d’éducation permanente/populaire. A l’intersection entre le théâtre et la conférence académique. J'adore apprendre, pratiquer, partager et surtout le faire librement et à plusieurs, je voyais alors là un exaltant défi et du plaisir à cutliver. Mais, malheureusement il y a trop souvent un mais, rapidement les premiers retours de plusieurs personnes que j'ai reçu ressemblaient à : 

> « _Mais tu n'es pas sur la liste officielle des conférenciers référencés[^6]. Tu ne peux pas proposer une conférence gesticulée comme ça !_ »

Et moi qui pensais que l'éducation populaire n'est pas l'éducation des peuples mais la possibilité aux peuples d'apprendre par eux-mêmes[^7], que cela rime avec libertés et libre connaissance.

Il faudrait pourtant plus pour me décourager. Manches retroussées, motivation chargée, dès le 4 décembre j'ai commencé à lire sur le sujet, à regarder plusieurs fois des vidéos, chercher des manuels. J'ai commencé à écrire des scénarii, j'ai contacté des personnes un peu partout en France. J'ai appris beaucoup, du moins je le crois. J'ai commencé à écrire des textes sur mes vécus depuis 2002. Mais (celui là c'est mon _mais_ à moi !) j'ai ressenti un dérangement. Je cherche vivre un moment _ensemble_ avec les personnes présentes et il m'apparait que pour bien des cas la conférence gesticulée est campée par une ou deux personnes sur scène face à un public posté passivement. 

Je me suis rappelé qu'avec quelques ami⋅e⋅s nous avions testé ce que nous appelons des anticonfèrence depuis 2013 à Marseille ou à Rennes ; tentant de remixer, de mélanger et d'arranger entre eux, à nos sauces, tous formats et toutes méthodes que nous trouvions intéressant⋅e⋅s, un peu comme Gregg Michael Willis (Girl Talk)[^8] le fait avec la musique.

J'ai donc décidé de m'inspirer des conférences gesticulées, et d'en emprunter quelques ficelles, sans m'y restreindre ni m'en estampiller officiellement pour tenter un format qui, je l'espère, comblera les personnes présentes et mes espoirs. Attention, je ne prétend pas révolutionner les formats de conférences quels qu'ils soient, je fais juste ma cuisine avec mes ingrédients, péraparations et assaisonnements.

Ce week-end du 11 au 13 janvier étant dédié à l'écriture (presque) finale et à des tests, j'ai bonne espérance de publier la première version de la documentation en tout début semaine suivante. 

Il me reste encore beaucoup de chemin et pain d'ici la fin de ce long exercice autour des libertés numériques du début d'années 2019.

### À la recherche de materia blanche

Comme écrit en début d'article, pour s'(auto)organiser lors de ce festival il faut compter sur ces resource propres, c'est à dire temps, savoirs, matériels, argent. 

Du temps j'en ai déjà aloué et je vais en dédier encore pas mal d'autres, ça je suis persuadé que vous en mesurez l'ampleur à la lecture ces lignes.

Du matériels j'en ai un minimum, un strict minimum. Il faudra que j'investisse environ 200 € en complément pour équiper les ateliers en materiel de biologie de base (éprouvettes, pipettes, gants...), produits chimiques ménagers, maquillage pour la reconnaissance faciale... 

Et il y a aussi toutes les documentations qui sont déjà en ligne et celles qui seront publiées libres sur le web... 

**Tout cela gratuitement**. 

Ce sont alors des sources et des ressources mises à disposition, des pages de lectures et codes gratuit⋅e⋅s, des recettes offertes. Peut-être que vous laissez un pourboire au café ou au restaurant ? Peut-être que laisseriez 1 € pour un journal ? Peut-être que vous donneriez un pièce pour une représentation gratuite ou à prix libre ? Et lorsque vous recevez des moyens d'apprendre ou de méthodes pour défendre votre vie privée et celles de vos proches vous faites quoi ?

C'est cela ma materia blanche après laquelle je cours pour les semaines à venir : vos soutiens ! Vos pourboires et vos partages de ces pages. 

Pour un don unique ou automatique chaque semaine à partir de 0,01 € sur liberapay (anonyme ou pseudonyme possible, sécurisé, libre et open source, transparent) :

<div>
   <script src="https://liberapay.com/Xav.CC/widgets/button.js"></script>
</div>

Ou sponsoriser par dons automatiques et réguliers du montant de votre choix sur OpenCollective (sécurisé et transparent) :

<div>
    <a href="https://opencollective.com/biononymous-and-bioprivacy">
        <img src="https://opencollective.com/biononymous-and-bioprivacy/tiers/sponsor/badge.svg?label=sponsor&color=brightgreen" />
    </a>
</div>


> « _En tant que dérivé du Lifestream, à l'intérieur même des matérias se trouvent les connaissances et les souvenirs de ceux qui ont vécu sur la planète. La matéria agit comme un lien entre ses utilisateurs et le Lifestream, cela permet à la mémoire associée à cette matière de se manifester sous une forme physique, par magie._ » <sup>[wikipedia](https://fr.wikipedia.org/wiki/Mat%C3%A9ria#La_mat%C3%A9ria_blanche)</sup>

En attendant la suite de cette aventure, j'espère vous lire ou/et vous voir très prochainement en ligne, à Rennes ou ailleurs. 

## Notes et références
 
 [^1]: Informations sur la conférence de Léna Dormeau <https://fdln.insa-rennes.fr/transplaner/conference-technologies-nbic-anthropomorphisme-et-appropriation-des-corps>
 
 [^2]: Si FF VII ou ファイナルファンタジーVI ne vous disent rien, voici l'explication de materia blance <https://fr.wikipedia.org/wiki/Mat%C3%A9ria#La_mat%C3%A9ria_blanche>
 
 [^3]: Si vous souhaitez en savoir plus sur le sujet, une page de ressources est à votre disposition <https://xavcc.frama.io/biononymous-bioprivacy-research>, cependant pour la séance du 4 février le contenu et la forme seront inédits. 
 
 [^4]: Lors de SudWeb 2018 avec un essai autour du cercle samoan faisant participer une partie du public <https://xavcc.frama.io/casper-sudweb-2018>, documentatation que je dois encore améliorer <https://framagit.org/Xavcc/xavcc.frama.io/issues>
 
 [^5]: Explication de la conférence gesticulée selon la Scop Le Pavé <https://www.lecontrepied.org/c-est-quoi-une-conference-gesticulee>, concept conçu par Franck Lepage <https://fr.wikipedia.org/wiki/Franck_Lepage>
 
 [^6]: Voici la liste dont je suppose l'officialité <https://www.ardeur.net/conferences-gesticulees-par-themes>
 
 [^7]: Je pense que c'est une paraphrase de Franck Lepage mais je ne parvient pas à sourcer exactement cette expression. Si vous des pistes, je suis preneur. 
 
 [^8]: La page wikipedia de l'auteur <https://fr.wikipedia.org/wiki/Girl_Talk> et son internet Illegal Track List <https://www.illegal-tracklist.net>