---
title: "Fdln, autodéfense bionumérique. Collecter, effacer ou brouiller de l'ADN. Et un peu plus..."
layout: post
date: 2019-01-23
image: /assets/images/opensourcebio.png
headerImage: true
tag: 
- Biononymous
- Bioprivacy
- Rennes
- Numérique
category: blog
author: XavierCoadic
description: Dites à ma maman que je vous ai écrit la recette bretonne qui chouchen tout et que je vous ai appris à vous défendre ;-)
---

Voici donc un troisième article autour du Festival des Libertés Numériques 2019, notamment concernant les séances que je prépare. Après [explication de mes intentions](/fdln-2019), puis un intermède sur la [boite de Pandore et l'ADN](/pandore-adn) et une [recette d'anticonférence gesticulée](/fdln-anticonference-gesticulee), il temps de rentrer dans la conception d'un atelier « Biopanique et autodéfense numérique ». Autrement écrit, il s'agit maintenant de vous donner contexte, scénario pédagogique, documentation et méthodes pour affronter les questions du fichage génétique.

> TL;DR Oui nous allons apprendre à travailler sur nos traces biologiques − Non ce n'est pas simple − Oui c'est vital et politique !

Dans un précédent article[^1] j'ai abordé certains des tenants du contrôle social que provoque des tests de paternité ou de géogénéalogie[^2]. Nous verrons dans cette nouvelle publication qu'il existe dans la conservation des échantillons d'ADN médico-légaux « une évaluation socio-éthique des pratiques actuelles dans l'UE » (Van Camp, N., & Dierickx, K. (2008). The retention of forensic DNA samples: a socio-ethical evaluation of current practices in the EU. Journal of Medical Ethics, 34(8), 606–610. doi:10.1136/jme.2007.022012). Nous travaillerons encore plus profondemment tout ceci lors des séances du Fdln. 

L'importance de concevoir un atelier _large public_ sur ces questions et manipulation technique réside dans les enjeux de mettre en capacité toute personne de s'informer, de connaître, et de transformer cela en savoirs par l'expérience et par la pratique, pour que le cas échéant cette personne puisse construire son avis critique et être en possession de moyen de se défendre dans des situations menaçant ses libertés. Vous pouvez même imaginer une réduction de vos droits à la mobilité liée au prélèvements biologique[^3].

> [Atelier « Biopanique et auto-défense bio-numérique »](https://fdln.insa-rennes.fr/agir/atelier-biopanique-et-auto-defense-bio-numerique/), 5 février, 19h, Rennes, prix libre

## Introduction à l’auto-défense bionumérique 

J'aimerais pour commencer rappeler les principes de base de la légitime défense qui n'est pas totalement identique à l'auto-défense, bien que l'auto-défense puisse, et doive parfois, se baser sur cette matrice juridique. La légitime défense est définie à l’[article 122-5 du Code pénal](https://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000006417218&cidTexte=LEGITEXT000006070719) qui dispose :

> « _N’est pas pénalement responsable la personne qui, devant une atteinte injustifiée envers elle-même ou autrui, accomplit, dans le même temps, un acte commandé par la nécessité de la légitime défense d’elle-même ou d’autrui, sauf s’il y a disproportion entre les moyens de défense employés et la gravité de l’atteinte.
N’est pas pénalement responsable la personne qui, pour interrompre l’exécution d’un crime ou d’un délit contre un bien, accomplit un acte de défense, autre qu’un homicide volontaire, lorsque cet acte est strictement nécessaire au but poursuivi dès lors que les moyens employés sont proportionnés à la gravité de l’infraction_ »

Pour résumer, afin de justifier une légitime défense en cas d’agression (elle permet que la personne ne soit pas condamnée en justice pour cet acte qui est normalement puni par la loi) il faut convenir à quatre points :

+ L'attaque est une **menace réelle et immédiate** : les menaces verbales ne constituent pas ce type de menaces.
+ L'acte de **défense est nécessaire**, vous ne pouvez pas faire autre chose, ni fuir avant de vous être défendu⋅e.
+ La réponse de défense doit être **proportionnée**. (ex: il n'est pas légitime de répondre à un coup de poing avec une arme)
+ La défense **advient au moment même** de l'agression et non après.

Donc dans une situation d'urgence, qui suppose une forme de surprise, sous une menace vous avez intérêt à agir très vite sans pour autant devoir faire n'importe quoi. Réfléchir, avant d'agir, en une seule fraction de seconde lorsque vous êtes acculé⋅e et menacé⋅e ? Pas simple...

Après coup, vous avez la loi et des avocat⋅e⋅s (commis⋅e⋅s d'office, mais demander **toujours un⋅e avocat⋅e**) pour vuos défendre.

Mais être préparé⋅es à l'éventualité de menaces ainsi qu'être entrainé⋅e⋅s aux réponses, immédiates et à long terme, à déployer vous donnera de nombreux avantages. Pour cela, vous devriez définir votre modèle de menaces[^4] puis envisager des précautions d'_hygiène_ numérique ou bionumérique dans certaines configurations[^5]. C'est la phase de prévisions dans votre conception de défense. Il faudra ensuite se préparer, s'entrainer, s'équiper, pour limiter, voir éviter, les menaces ou être capable d'y répondre dans une situation d'urgence immédiate. De la même manière que des cours d'auto-défense existe pour que des personnes se défendent lors d'agression dans la rue. C'est la phase de prévention de votre coneption de défense. Il faudra aussi avoir quelques connaissances de vos droits et vos obligations légales. C'est le [moyeu](https://fr.wikipedia.org/wiki/Moyeu) de votre mécanique de défense.

Ainsi, ces phases conçues et articulées entre elles consituent votre _machine_ d'auto-défense ayant pour châssis vos savoirs, savoir-faire et savoir-être. Défendre consiste alors plus à cultiver, jardiner et construire, qu'a réagir pour détruire.

Pour acquérir tout ceci et l'entretenir quoi de mieux que des rencontres ? Et des documentations, donc des connaissances, sous licence libre ? Il y a pourtant encore des obstacles à franchir avant d'envisager de fournir au plus grand nombre les moyens d'apprendre et de s'équiper par soi-même. Dans la _sphère_ numérique c'est compliqué, heureusement il y des initiatives parmi d'autres comme le festival des libertés numériques. Malheureusement lorsque numérique s'entremêle avec les fils du biologique c'est aujourd'hui de l'ordre du cosmique. D'abord car partout _y a Jean-Lou de Disruptcom_ qui parle de digital. Alors que certes numérique et [dermatoglyphes](https://fr.wikipedia.org/wiki/Dermatoglyphe) sont souvent en _contact_, alors que l'on pourrait aussi faire des métaphores entre les empreintes, mais la réalité est que le numérique fait des choses et ce mot _dit_ des choses tout comme le doigts et son empreintes digitales fait de choses[^6], comme déposer votre ADN partout où le posez[^7].

Dès que l'on aborde le numérique ou le Vivant il y a une [cosmogonie](https://fr.wikipedia.org/wiki/Cosmogonie), c'est-à-dire un système d'informations qui nourrit et véhicule des légendes et des imaginaires, des mythes et des légendes. Cela procède parfois à du _bien_ pour les individus, par exemple nous avons peut être besoin d'entretenir l'image de futurs désirables. Souvent cela entraîne des biais qui sont autant de difficultés lorsque l'on veut résoudre sérieusement des problèmes, comme pour l'écologie[^8] ou de se défaire de _prophètes_ du contrôle social[^9].

Surtout, lorsque vous proposez des ateliers pour apprendre à se défaire _du contrôle_ et à effacer ou brouiller des traces, vous faites peur à des personnes qui _aiment_ leur cosmogonie, vous recevez d'autres un paquet d'étiquettes sur le dos avec des dénominations farfelues.

Voilà pourquoi un scénario pédagogique pour ces types d'ateliers est d'importance égale aux contenus de ces ateliers. C'est aussi un petit bout de (auto)défense et de construction.

## Scénario pédagogique de l'atelier

> Points Importants : Des _slides_ y'en aura pas, ça endort. De la documentation sous licence libre devrait être de l'éthique de toutes et tous intervenant⋅e⋅s.

+ La durée prévue : 2h
+ Nombre max de personnes : 20. grand maximum pour conserver les espace-temps entre facilitateur et participant⋅e⋅s

La pratique par travaux d'expérimentations n'empêche pas de travailler avec sérieux et profondeur les sujets société qui enserre les enjeux de la thématique, par par exemple des _écoles_ qui conditionnait les femmes à êtres des _bonnes_ obéissante de cuisine et la place de femmes dans les sciences moderne[^10]. le contenant contraint les contenu⋅e⋅s⋅

Il est alors possible de se fixer des objectifs d'ateliers tels que : 

+ autonomie des participant⋅e⋅s, e
   + en fournissant des support leur permettant de _faire_ par elles et eux-mêmes
   + en fournissant le matériels de base
   + en introduisant en moins de 5 minutes la séance
   + en invitant à constituer de petits sous-groupes de pratique
   + en facilitant à chaque bloquage d'un sous groupe
+ co-conception de moification du protocole de base, pour apprendre l'expérience
+ co-réalisation
+ documentation collective évolutive, pad ou wiki ou support papier ou interview
+ envisager le potentiel et la dimension éthique des pratiques
+ se confronter à ses “zones d’ignorance”
+ créativité / idéation

Sur un format plus long, il est envisageable de multiplier ses objectifs sans pour autant alourdir le contenu de la séance[^11]. Les étapes et manipulations de bases peuvent se faire en moins de 5 minutes pour collecter et extraire de l'ADN[^12], ce qui laisse le loisir de ralentir le rythme pour y insérer des phases _agora_ d'interrogations collectives sur les items que les participant⋅e⋅s feront remonter. Les personnes en charge de facilitation doivent ainsi se préparer à beaucoup d'éventualités et savoir réagir dans l'inconnu, y compris et surtout avouer ses propres méconnaissances (qui pourra mener à une proposition : « Cherchons réponse ensemble).

Ainsi, l'atelier en question pour le FDLN se déroulera :

1. Intro au sujet et à al séance en moins de 5 minutes
2. Formation de petits groupes
3. Démarrage de manipulation de collecte et extraction d'ADN
  + Questions u fil de l'eau
  + Prises de notes
4. Pause et retour d'impressions
5. Pratiques pour Brouiller, obfusquer[^13], effacer[^14], des traces génétiques
6. Forum ouvert ou _Open Bar_ pour travailler :
  + Les questions de perte d’intimité biologique
  + Le Vivant comme un commun ou le Vivant confisqué
  + Le contrôle social par privatisation du Vivant

D'expérience, je  peux vous assurer de tonnes de questions qui émergeront dans les esprits des personnes présentes. Toute la difficulté est de concevoir les conditions pour qu'elles s'expriment. J'utiliserais un Code De Conduite[^15] affiché clairement et je profiterais d'être détaché de la posture _de celui qui fait_ pour aller discuter plus _resserré_ avec les personnes qui ne souhaite ou ne peuvent pas prendre parole devant un groupe pour recueillir leurs questions.

> Toute notes, œuvres, qui sera écrite, conçue, lors de cet atelier pourra être publiée après choix collectif de personnes en présence de la licence appliquée contenant mention de ces personnes comme dépositaires ;  et consensus sur le fait de publier en ligne. Dans l'impossibilité de réunir ces deux conditions, les compositions collectives resteront non publiées.

> Toute personne a le doit de refuser d'apparaître en image et/ou dénomination sur tout support lors de cet atelier. Aucune photo, vidéo, prise de son, ne peut être prise sans consentement explicite des personnes y figurant.

## Les activités de l'atelier

### Extraire de l’ADN soi-même ou à plusieurs avec des produits courants

c'est une manipulation simple, possible dès 7 ans avec un adulte, en utilisant des produits ménagers, une [documentation est déjà disponible](https://notecc.frama.wiki/norae:biologicus:bioprivacy_note-3). L'objecttif est de démystifier le rapport de l'humain à l'ADN et de permettre à toute personne présente de la reproduire chez elles et/ou avec d'autres personnes.

Passez ensuite par une proposition « Si nous le refaisions maintenant avec vos cheveux, vous êtes d'accord ? » est une transition souvent utilisée.

### Brouiller, obfusquer, effacer, des traces génétiques

Effacer de traces biologiques peut être aussi présenté et réalisé tout aussi simplement, [documentation](https://notecc.frama.wiki/norae:biologicus:bionprivacy_note-4-adn), mais il convient d'aborder le plus grand nombres de méconnaissances que cela porte, parfois induites par des séries télé ou des _initiatives_ de personnes publiées en ligne sans faire part de la réalité d'avancée des sciences, de la persistance de traces biologiques ou des législations.

De plus une stratégie inspirée d'une_[Bombe à décompression](https://pl.quic.fr/notice/1714203)_, dédiée à surcharger l'espace en traces biologiues, serait bien plus efficace que celle par vaporisation manuelle sur zone. 

### Les questions de perte d’intimité biologique

> « _Car c’est aussi ce qu’amènent ces tests, ils nous prouvent que nous sommes le fruit d’une histoire, chacun de nous est en fait un métis, au sens où nous sommes tous le fruit de l’union des gamètes de deux personnes différentes. Peut-être suis-je naïf mais je crois que la génétique est par essence anti-raciste, tout comme les mathématiques et la biologie : mieux on comprend comment tout cela fonctionne et moins on se laisse abuser par les fausses évidences._ » Jean-Noël Lafargue, « [Comment j'ai fais du tourisme génétique](https://hyperbate.fr/dernier/?p=39292)

C'est après les manipulations, après les expérimentations, que viens le temps de travailler à partir des remontées des _praticiennes et praticiens_ les questions d'intimité, de ce qui nous est commun dans notre individualité, de qui nous est collectif dans notre humanité.

Les problématiques abordées ne sont par restreinte au genre humain. Comment pourriez-vous cultiver quoique soit si les bactéries du sol, les graines des végétaux ou les pollen des fleurs appartenaient à un groupe privé que le marchande ?[^16]

### Le Vivant comme un Commun ou le Vivant confisqué

Cette phase toujours discutée oralement vise à ouvrir plus largement les considérations précédentes et d'envisager un concept : [Les Communs, y compris les communs non-humains](https://scinfolex.com/?s=communs). 

Le fichage génétique n'étant qu'une des possibilités, nous pourrons aborder l'identifaction par empreintes digitales ou faciales, ou encore par _empreintes_ de votre biome bactériens, ou par votre façon de marcher.

Il s'agit d'une agora _critique_ dans laquelle chaque personne doit pouvoir les matériaux pour forger sa compréhension et son avis. Les facilitatrices et facilitateurs ne sont pas en posture d'_imposer_ une vision prédominante.

### Le contrôle social par privatisation du Vivant ?

Cette dernière étape de conclusion tend non pas à fermer les réflexion et les échanges mais à porter en guise de _fin de séance_ des _graines_ sociologique et/ou juridique à la production collective. Surtout que ces problèmes et les alertes ne datent pas d'aujourd'hui, nous avons alors, nous citoyennes et citoyennes un dette de connaissance, de techniques, et surtout une dette d'intérêt sur ce enjeux.

Par exemple, ce rapport de 2007 « The retention of forensic DNA samples: a socio-ethical evaluation of current practices in the EU » (N Van Camp, K Dierickx)

> « _La collecte et l'entreposage de grandes quantités de matériel biologique par les services de police nécessitent une réglementation spécifique concernant leur utilisation et leur durée de conservation.  Étant donné que les politiques nationales des États membres de l'UE concernant ces questions sont diverses et parfois même inexistantes[...]. la protection de la confidentialité des données génétiques.  Recommandation n° (92), le Conseil de l'Europe constitue un bon point de départ. Toutefois, comme il ne contient pas de lignes directrices concrètes sur la façon dont les échantillons peuvent être prélevés et comme il ne s'agit pas d'une loi contraignante, il existe un le besoin urgent d'une nouvelle initiative au niveau de l'UE. Dans ce contexte, une suggestion séduisante a été formulée par la Direction de la protection des données Groupe de travail. Dans une évaluation de la directive 95/46/CE, de la criminalistique par l'emploi de l'ADN a été abordée explicitement en indiquant qu'i convient également de tenir compte du statut juridique de l'ADN et des échantillons [^17]. Ayant à l'esprit que la présente directive crée le droit impératif, il semble compréhensible que le député Les États tiennent à ce que les questions de justice et de sécurité n'entrent pas dans le champ d'application de la Convention. le champ d'application. Néanmoins, le caractère sensible de l'information génétique demande une exception en ce qui concerne la réglementation de l'ADN par échantillons prélevés à des fins médico-légales_ »

Vous trouverez par vous même, ou dans les ressources fournies dans cet article blog, de nombreuses ressources pour illustrer l'ancienneté des problèmes et leurs évolutions.

« Oui, mais on peut anonymiser car... » Paf le chien !

En 2013, un jeune biologiste informaticien nommé Yaniv Erlich a choqué le monde de la recherche en montrant qu'il était possible de démasquer l'identité des personnes inscrites dans des bases de données génétiques anonymes en utilisant seulement une connexion Internet. Les décideurs ont réagi en limitant l'accès aux banques de données génétiques biomédicales anonymisées[^18]. Mais les problèmes continuent d'empirer, juste cela empire sur d'autre serveurs avec des configurations technologiques différentes et jamais totalement infaillibles. 

Vous pourrez alors aborder les [problèmes de la boite de Pandore](/pandore-adn).

## Et après...

À chaque personne de décider ce qu'elle fera de ces apprentissages et de ces questions. À chaque lectrices et lecteurs d'en faire autant avec ces ressources publiées (en respectant les licences libres attribuées).

Pour ma part, dès aujourd'hui je vous prépare un quatrième article de la série festival des libertés numériques, comme promis [ici](/fdln-2019), ainsi que les documentations, notes, méthodes et les codes de programmation, pour faire une séance similaire mais sur la reconnaissance faciale qui sé déroula :

**[Atelier reconnaissance faciale et morphologique de masse](https://fdln.insa-rennes.fr/agir/ateliers-protegez-votre-vie-privee-et-passez-au-libre/), 7 février, 18h, INSA Rennes, gratuit.**

> _En attendant, la demande de don en ligne, lancée le 11/01/19, pour soutenir cette démarche a atteint : **Zéro €**_


## Notes de bas de page

[^1]: Confisquer le rapport à soi dans l’épaisseur d’un dossier jamais clos. Vous dire qui vous avez été, comment vous êtes et qui vous devrez être. Non pas mutiler, non pas opprimer ou réprimer l’individu comme on le crie si naïvement : le fabriquer. <https://xavcc.frama.io/pandore-adn>

[^2]: l'affaire des 2 jumelles hétérozygotes avec _des ancêtres différents_ être révalatrice de ces manipulations sociales. Voici quelques notes à ce sujet <https://notecc.frama.wiki/norae:biologicus:bioprivacy_note-1-adn>

[^3]: Une récente campagne moquant l'administration Trump par une compagnie de voyage aérien promettant des réductions contre tests ADN, notes <https://notecc.frama.wiki/norae:biologicus:bioprivacy_note-adn>

[^4]: l'article de blog de Genma pourrait vous donner de bonnes pistes pour démarrer ce travail, mais vous devrez l'adapter à vos univers <https://blog.genma.fr/?Quel-est-votre-modele-de-menace>

[^5]: encore le blog de Genma en 2016 qui offre un guide avec de bonnes bases d'hygiène numérique qui peuvent être un départ pour d'autre formes dérivées hygiène <https://blog.genma.fr/?Guide-d-Hygiene-numerique-version-2016>

[^6]: notes sur De la formation à l'usage des empreintes digitales : dermatoglyphes <https://notecc.frama.wiki/norae:biologicus:bioprivacy_note-1-empreintes>

[^7]: « Des chercheurs néerlandais ont testé 105 objets publics : mains courantes d’escalators, poignées de portes de toilettes publiques, poignées de paniers, pièces de monnaie23. 91% contenaient de l’ADN humain, parfois celui d’une demi-douzaine de personnes. Même les objets qui nous sont intimes - les aisselles de nos chemises, disons - peuvent porter l’ADN d’autres personnes, ont-ils trouvé » <https://xavcc.frama.io/pandore-adn>

[^8]: notes sur « Biomasse, poids carbone et place de l'humain » <https://notecc.frama.wiki/norae:biologicus:ecologie_cas-1-a>

[^9]: Notes sur la critique de la collapsologie et le contrôle social <https://notecc.frama.wiki/norae:biologicus:ecologie_note-1-c>

[^10]: Depuis 2016 nous avons réaliser plusieurs ateliers en France « Biopanique cuisine et féminisme » <https://notecc.frama.wiki/atelier:biopanique> et les téalités du "Home Economics" <https://en.wikipedia.org/wiki/Home_economics>

[^11]: Comment préparer une séance d'une journée entière <https://xavcc.github.io/biomimicry-method>

[^12]: les notes sur les manipulations et d'autres pratiques plus avancées <https://notecc.frama.wiki/norae:biologicus:bioprivacy_note-3>

[^13]: Brouiller des traces biologiques, recettes, manipulations et questionnements <https://notecc.frama.wiki/norae:biologicus:bioprivacy_note-5-adn>

[^14]: Notes sur l'effacement des traces biologiques entre réalités et croyances, simplicité et difficultés <https://notecc.frama.wiki/norae:biologicus:bionprivacy_note-4-adn>

[^15]: Nous avons utilisé ce code de conduite dans des évènements tels que IndieCamp, hackathon... ou encore sur des wiki <https://notecc.frama.wiki/cgu#code_de_conduite>

[^16]: Un ensemble de ressources sont à votre disposition ici pour aborder ces problèmes <https://xavcc.frama.io/biononymous-bioprivacy-research>, comme par exemple un robot abeille de WalMart <https://xavcc.frama.io/biononymous>

[^17]: Article 29 Data Protection Working Party, Working document on genetic data (17 March 2004) III,§1. Available at: http://ec.europa.eu/justice_home/fsj/privacy/docs/wpdocs/2004/wp91_en.pdf (accessed 11 June  2008).

[^18]: article de Wired <https://www.wired.com/story/genome-hackers-show-no-ones-dna-is-anonymous-anymore>

