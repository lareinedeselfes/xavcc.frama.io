---
title: "Un VPN pour protéger vos communications réseaux comme si vous achetiez un préservatif ?"
layout: post
date: 2018-09-18 18:48
image: https://criticalengineering.org/projects/vending-private-network/images/title3.jpg
headerImage: false
tag:
- hsociety
- Numérique
- Libertés
category: blog
author: XavierCoadic
description: un tunnel sécurisé à l’intérieur d’un réseau en latex ?
---

Une infection, ou plutôt des maladies et infections viralement transmissibles se propageraient au sein de nos libertés et de nos humanités ? Censure, espionnage, surveillance de masse et contrôle individuel ? Ce sont certaines de questions que des membres du The Critical Engineering Working Group, Julian Oliver, Danja Vasiliev, semblent vouloir traiter par les prismes artistiques, technologiques et du design. 

Ce sont aussi ces deux personnes qui avaient rédigé avec Gordan Savičić [The Critical Engineering Manifesto](https://criticalengineering.org/) en 2011.

Par respect pour le courriel reçu et le travail des concerné⋅e⋅s, je vous retranscris en français leur synthèse d'introduction.


```txt
Le groupe de travail sur l'ingéneirie critique est heureux de présenter un nouveau travail : 
//---------------------------------------------------------------------> 

Vending Private Netwrok (2018) 

Les réseaux privés virtuels en tant qu'infrastructure financée par le secteur public 

Les réseaux privés virtuels (VPN) sont devenus de plus en plus demandés ces dernières années, 
fournissant le chiffrement des routes via des réseaux hostiles. 
En Chine, au Vietnam, en Turquie et au Pakistan, ils servent également à atténuer la censure gouvernementale, 
de sorte que les sites étrangers autrement bloqués par des pare-feu d'État sont mis à la disposition 
des utilisateurs de VPN (Twitter, Facebook, Wikipedia, sites activistes et bibliothèques numériques étant les plus courants). 

Vending Private Network se présente sous la forme d'un distributeur automatique de préservatifs, 
comme ceux que l'on trouve généralement dans les toilettes publiques, les boîtes de nuit et les bars. 
Equipé de boutons mécaniques, d'une fente à monnaie et de ports USB, 
il offre 4 routes VPN, chacune ornée d'un graphique animé représentant une destination fantaisiste. 

Le public est invité à insérer une clé USB dans la fente, une pièce de monnaie (1 livre ou euro) dans la machine 
et à sélectionner une destination VPN en appuyant sur un bouton mécanique. 
Ce faisant, un fichier de configuration VPN unique est alors écrit sur la clé USB. 
Des instructions spéciales (sous la forme d'un fichier README.txt) sont également copiées, 
expliquant comment utiliser le VPN dans un mode spécial "gainé" qui échappe aux méthodes de détection 
(à savoir Deep Packet Inspection, ou DPI) utilisées par les entreprises 
et les administrateurs d'infrastructure contrôlés par l'Etat. 
C'est le seul moyen connu pour lutter contre les pare-feu contrôlés par l'État. 

https://criticalengineering.org/projects/vending-private-network/ 

//<--------------------------------------------------------------------

Vending Private Network a été développé par J. Oliver et D. Vasiliev, 
représentant le Critical Engineering Working Group, 2018.

Vending Private Network est actuellement exposé à Furtherfield, 
Londres, dans le cadre de 'Transnationalisms', organisé par James Bridle, 
et sera présenté dans 3 autres villes du monde. https://www.furtherfield.org/events/transnationalisms/
```

Le parralèle entre préservatif et VPN peut mener à de vives discussions, tout comme le texte qui accompage l'œuvre, je n'ai pas envie de les entamer dans ce billet blog. Je préfère bien plus vous inviter à en discuter ensemble en campagne ou en ville, dans un café ou au travail, dans un salon de discussion d'Internet ou sur un réseau social. Ou même du Critical Ingeenering Manifesto...

Pour l'instant, je réflechis sur cette œuvre et sur où, quand et comment aller la triturer. 

