---
title: "La boite de pandore des fichiers génétiques, regard croisé entre France et USA par l'histoire du meurtrier du Golden State"
layout: post
date: 2019-01-14 
image: /assets/images/pandora.jpeg
headerImage: true
tag:
- Bioprivacy
- Biononymous
- hsociety
- Libertés
category: blog
author: XavierCoadic
description: Donne moi ton ADN et je te dirais qui construira ta prison
---

Pendant plus de 40 ans, le Tueur du Golden State [^1] a échappé aux autorités. Alors que la police s'efforçait de résoudre l'affaire, Joseph James DeAngelo aujourd'hui soupçonné d'être le cambrioleur, violeur et meurtrier en série qui a terrorisé l'État dans entre 1970 et 1980 - a mené une vie tranquille dans une banlieue de Sacramento. Dans les années 1980, alors que l'analyse génétique en était encore à ses débuts, les laboratoires de criminalistique avaient besoin d'un échantillon de liquide corporel - habituellement du sang, du sperme ou de la salive - pour établir un profil génétique. 

Pendant ce temps, les échantillons génétiques prélevés sur les scènes de crime se trouvaient dans un placard. Il n'y avait pas de correspondances. Sciences et techniques n'étaient pas ce qu'elles sont aujourd'hui, les basees de donénes moins fournies et plus difficiels à croiser, la disruption n'était descendue sur Terre. Ces traces biologiqes semblaient inutiles - jusqu'à ce qu'un enquêteur envoie le 19 avril 2018, via un site internet peu connu dédié à généalogie, des échantillons des cet ADN pour analyse et recoupement à une entreprise américaine. 

J. DeAngelo, 72 ans, a été arrêté le 25 avril 2018, ce qui est présenté comme une avancée stupéfiante pour les forces de l'ordre. Les autorités du comté de Sacramento l'ont accusé de deux chefs d'accusation de meurtre, et d'autres comtés ont rapidement suivi. Il est heureux que des supects soient confrontés à leurs accusations, que des personnes reconnues coupables purgent leur peine, pour les victimes, pour les familles et proches de victimes, pour le fonctionnement de la société dans son arrangement actuelle. Mais l'utilisation par les forces de polices d'un service généalogique en ligne de « source ouverte » posent de nombreuses questions, avec des fonctionnements similaires à l'entreprise et site internet 23andMe, qui a subi des fuites de données[^2] ou encore Ancestry, ayant aussi laissé fuiter des données[^3]. À travers le mondes ces entreprises inquiètent en raison de la vente à des tiers d'informations sensibles sur les clientes et clients. Cette marchnadisation amène des répercussions considérables sur la vie privée de toute la population et pas uniquement sur celles des personnes qui ont fourni un échantillon biologique. 

> « _Beaucoup de gens ne savaient pas que d'autres personnes pouvaient accéder à ces sites_ »

Avait déclaré Arthur Caplan, directeur de la Division de l'éthique médicale à l'École de médecine de l'Université de New York, dans le San Franciso Chronicle[^4].

> « _C'est un rappel que ces entreprises ont non seulement des tiers et des autorités légales qui viennent, mais qu'elles revendent parfois leurs données. Ils ne se contentent pas de vous dire si vous êtes Lituanien ou Portugais. Ils tirent beaucoup d'argent de l'information sur la revente._ »

C'est un des plus petits sites internet généalogique en Floride appelé GEDmatch<sup>[Wikipedia](https://fr.wikipedia.org/wiki/GEDmatch)</sup>, _la start-up locale_, qui a mené les enquêteurs à l'homme aujourd'hui accusé d'être le tueur de Golden State et le violeur de East Area, lié à 12 meurtres et environ quatre douzaines de viols. Ce site gratuit propose aux personnes de trouver des membres de leur famille et d'entrer en contact avec eux. Il permet aux utilisatrices et utilisateurs de comparer des échantillons d'ADN avec celles et ceux qui ont également téléchargé des informations génétiques via des plateformes comme 23andMe, élargissant ainsi le volume de données.

Dans ce cas de figure, c'est un pouvoir de police qui s'est servi de données, très sensibles, d'entreprises privées sans passer par la voie d'une demande à la justice. Cela pose un problème de protection des individus, du droit à la présemption d'innocence (si je fais fausse route car ceci n'et pas ma spécialité, je vous invite à proposer une amélioration [ici](https://framagit.org/Xavcc/xavcc.frama.io/issues) ). Cela pose également un problème sur la séparation des pouvoirs entre le législatif, l'exécutif et le judiciaire. Ce qui est pourtant un principe fondamental des démocraties représentatives alors que les régimes dictatoriaux recherchent une concentration des pouvoirs.

L'étendue du problème est encore plus complexe que l'initative des policiers américains couplée aux vacuités des start-ups impliquées. En réalité, 23andMe est l'expression d'une surveillance de masse qui n'a que faire des frontières des états. L'entreprise a été fondée par Linda Avey, Paul Cusenza et Anne Wojcicki en 2006 pour fournir des tests génétiques et des services d'interprétation aux consommateurs individuels. En 2007, Google a investi 3 900 000 $ dans l'entreprise, avec Genentech, New Enterprise Associates et Mohr Davidow Ventures. Wojcicki était marié à Sergey Brin, cofondateur de Google à l'époque. Rappelons que Google est l'une des entreprises qui collectent le plus d'informations personnelles au monde. Les résultats des tests ADN à domicile des 5 millions de clients de 23andMe seront maintenant utilisés par le géant pharmaceutique GlaxoSmithKline pour concevoir de nouveaux médicaments, annoncent ces deux sociétés[^5].

## Qu'en est-il en France des fichiers génétiques ? 

USA et France n'ont pas les mêmes legislations, ni les mêmes procédures.

Rappelons qu'en France, pays que se veut _start-up Nation_, vous risquez 3750 euros d'amende en commandant ce genre test ADN[^6]. Les tests de filiation génétique ne sont autorisés que dans le cas de mesures d’enquête ou d’instruction lors d’une procédure judiciaire (loi du 7 août 2004).
D'après la loi Informatique et Liberté (CNIL 1978), en France tout responsable de traitement informatique de données personnelles doit adopter des mesures de sécurité physiques (sécurité des locaux), logiques (sécurité des SI) et adaptées à la nature des données et aux risques présentés par le traitement. Ne pas protéger correctement des informations à caractère personnel, et sensibles est puni de 3 ans d’emprisonnement et de 100 000 € d’amende ([article 226-22 du code pénal](https://www.legifrance.gouv.fr/affichCodeArticle.do;?idArticle=LEGIARTI000006417984&cidTexte=LEGITEXT000006070719)). Le non-respect de l’obligation de sécurité est sanctionné de 5 ans d’emprisonnement et de 300 000 € d’amende. ([article 226-17 du code pénal](https://www.cnil.fr/fr/les-sanctions-penales)).

Cela devrait nous aider à nous protéger. Seulement interdire ou sanctionner n'est pas toujours synonyme de protection. Lorsque les bases de données génétiques couvrent 2 % de la population, presque tout le monde peut être identifié.

> « _Même si vous n'avez jamais fait de test ADN, celui d'un parent éloigné pourrait révéler votre identité._ » Washington Post, Carolyn Y. Johnson, October 11 2018[^7].

En France, le fichage des individus par ADN ou biométrique, le rôle de la police, la législation et les velléitées d'élu⋅e⋅s, sont actuellement des sujets brûlants[^8] sur lesquels l'augmentation constante du Fichier National des Empreintes Génétiques (FNAEG) et les multiples alarmes sonnées ne semblent pas infléchir la tendance actuelle vers une extême surveillance et fichage des populations. Cette glissade vers une panoptique biologique généralisée _made in France_ allant même jusqu'à tenter le fichâge des individus n'ayant pas la nationalité française passant par son sol, enfants y compris demandant refuge et asile[^9].

> « _Dans le fichier national des empreintes génétiques: on peut dire des choses. Repérer les gens et tirer des informations sur l’origine et l’état de santé_ » Commission Nationale Informatique et Libertés (CNIL)[^10]

Serait-ce là une contradiction affichée entre la volonté de ficher _l'étanger_ et de protèger le _français_ ? Ou un aveu de ficher tout le monde mais avec des discours et marchnadisation différents[^11] ?

> « _« Les données génétiques ne sont pas des données personnelles comme les autres. Particulièrement intimes et potentiellement discriminantes, ces données bénéficient d’un statut légal très protecteur. Mais, alors que leur utilisat° tend à se banaliser, les enjeux éthiques n’ont jamais été aussi importants : risques de manipulation génétique, de discrimination, maitrise de son patrimoine génétique, marchandisation »_ » CNIL, 13 Septembre 2017[^12]

Par exemple, De 2002 à 2016, le nombre de personnes fichées au FNAEG est passé de 4 369 à près de 3,5 millions ! Soit autour de 5% de la population française.[^13]

## Revenons au USA

L'entreprise et leur site internet GEDmatch est peu connu en dehors du monde des amateurs et amatrices de généalogie. Une personne, d'un servcie de police par exemple, pourrait créer un compte sous un faux nom et télécharger les données de l'échantillon qu'ils avaient de l'assassin présumé en utilisant, puis espérer qu'un parent - un cousin, un grand-père, un frère ou une sœur - ait également téléchargé des informations sur le site.

Un tel lien a en effet été établi dans le cas du tueur du Golden State. Ce qui a produit une cohorte de fiches d'individus qui a ensuite été soigneusement réduite à une poignée de suspects potentiels. Le profil génétique de DeAngelo _matchait_ fortement. Le bureau de shérif du comté de Sacramento surveilla sa maison à Citrus Heights dans l'attente qu'il jette aux ordures un objet avec son ADN dessus. Après deux tentatives et tests sur deux prélèvements, c'est le second échantillon, qui, selon les enquêteurs, correspondait parfaitement.

Bien que l'affaire et ses procédés ouvrent de nouvelles possibilités pour de futures enquêtes policières, elle révèle d'importantes préoccupations en matière de protection de la vie privée dans l'industrie du dépistage génétique. Une personne qui paie pour ce genre de service via site Web pourrait exposer ses proches et ses parents éloignés - passés, présents et futurs - à une enquête policière éventuelle. Conduisant toute personne à construite gratuitement, et presque inconsciement, les prissons pour ses ancêtres, pous sa descendance, pour soi-même. 

Curtis Rogers, un asscoié de GEDmatch, avait ensuite déclaré que l'entreprise et leur site internet n'avait pas été alerté avant que les détectives utilisaient leur service pour rechercher le tueur de Golden State. Les responsables de l'entreprise n'ont pas non plus coopéré avec la police, a-t-il dit.

Les représentants de 23andMe et d'Ancestry avaient eux déclarés qu'ils ne communiqueraient pas de données personnelles à la police à moins d'y être contraints par une procédure légale valide. Mais les personnes qui s'inscrivent pour utiliser les services de telles entreprises sont informées par de très longues, illisibles et souvent incopréhensibles Conditions Générales D'utlisation (CGU). Dans le usages et dans les faits, les données des utilisatrices et utilisateurs peuvent être utilisées d'une manière qu'elles ou ils n'avaient jamais envisagées lorsqu'elles ou ils envoient leur échantillon biologique pour analyse ADN.

Les utilisations possibles de ces données peuvent être d'une grande portée, dont nous ne mesurons pas tous les aboutissants encore aujourd'hui, puisque les kits génétiques gagnent en popularité et de plus en plus d'informations sont collectées et stockées, y compris avec des métadonnées non génétiques mais qui renforcent la puissance des usages de ces bases de données, comme par exemple des données issue de collecte massive et ultra personnelle de chez Google.

Des entreprises comme 23andMe, qui vendent des kits à 165 € qui _décodent_ la santé des personnes et les données généalogique, sont devenues l'un des plus importants dépôts privés d'ADN au monde. Entre le Black Friday et Cyber Monday l'an dernier, l'entreprise a vendu 1,5 million de kits de test. L'accès à certaines de ces données a déjà été vendu à des sociétés telles que des sociétés pharmaceutiques, d'après un déclaration de peronnel de 23andMe admis en 2015<sup>Source manquante</sup>.

« Il n'y a pas d'interdiction et de nombreuses entreprises disent qu'elles peuvent encore vendre vos renseignements personnels à d'autres entreprises », avait déclaré M. Schumer, sénateur américain en 2017. « Il s'agit d'informations sensibles, et ce que ces entreprises peuvent faire avec toutes ces données, nos informations sensibles et les plus profondes, votre génétique, ne sont pas claires et, dans certains cas, ne sont ni justes ni éthiques. »

Aux USA les retombées de l'affaire DeAngelo ont déjà touché la communauté des généalogistes. Kitty Cooper, une bénévole de GEDmatch, rappela au San Franciso Chronicle que des internautes avaient laissé des commentaires sur son [blog de généalogie](https://blog.kittycooper.com/) pour savoir s'ils devaient supprimer leurs comptes ou les marquer comme étant juste dédié à la _recherche_. « Bien sûr », Cooper dit-elle en réponse. Mais elle leurs demande aussi : Si leur ADN pouvait aider à résoudre un crime odieux, ne voudraient-ils pas aider ?. Force de conviction et de vente en cherchant à embarquer l'affect...

La police a pu attraper DeAngelo grâce au grand nombre de personnes qui ont utilisé des kits de prélèvements dometsique pour analyse ADN, et les ont envoyées via des sites Web généalogiques, avait déclaré Jennifer Lynch, avocate à l'Electronic Frontier Foundation (EFF). Elle reprend d'ailleurs un [article du Daily Journal sur cette affaire](https://www.eff.org/fr/deeplinks/2018/05/distant-relatives-arent-only-ones-looking-your-dna-geneology-sites-law-enforcement) pour renforcer les sérieux avec lequel l'EFF considére les problématiques de vie privée, menances sur les libertés autour de la génétique et de la biométrie[^14].

La société 23andMe, fondée en 2006, déclare sur son site Internet qu'elle compte plus de 5 millions de clients. GEDmatch, fondé quatre ans plus tard, a collecté environ 950 000 "kits" d'ADN.

Pour J.Lynch il est préoccupant que les organismes en responsabilité de l'application de la loi puissent utiliser de tels sites Web de la même façon que les utilisateurs et utlisatrices le peuvent. Le fait de savoir que la police peut avoir accès aux données pourrait décourager les gens d'échanger des renseignements personnels sur ces sites, et ce, pour une bonne raison, a-t-elle ajouté.

« Pour chaque exemple que nous avons d'un tueur en série attrapé, nous avons aussi des gens qui ont été pris à tort », déclare J. Lynch au S-F Chronicle, notant un cas en 2014 dans lequel un homme de l'Idaho a été lié à tort à un meurtre vieux de 20 ans sur un site appartenant maintenant à Ancestry. « À quelle fréquence les forces de l'ordre utilisent-elles cette tactique ? Et quel genre de procédure légale ont-ils avant d'avoir cette base de données ? »

> « _La diffusion des données génétiques pourrait également avoir une incidence sur l'assurance-maladie future, ce que de nombreux utilisateurs ne prennent pas en considération. Le téléchargement d'informations génétiques sur des sites comme GEDmatch n'est pas couvert par la Health Insurance Portability and Accountability Act, ou HIPAA, a déclaré Pam Dixon, directrice générale du World Privacy Forum._ » [SF Chronicle, 29 avril 2018](https://www.sfchronicle.com/crime/amp/Arrest-of-suspected-Golden-State-Killer-through-12872258.php)

Lorsque des individus téléchargent des informations sur des troubles génétiques dont ils souffrent, comme la maladie de Huntington ou la maladie D'Alzheimer, cela pourrait exposer publiquement leurs proches, « ce qui les rendrait sujets à des taux d'assurance plus élevés par exemple », répète Pam Dixon. L'information pourrait également jouer un rôle dans les affaires de garde ou dans les situations où une personne est un héritier légitime d'une propriété ou d'un héritage, a-t-elle dit.

« C'est une boîte de Pandore », pour P. Dixon directrice générale du World Privacy Forum. « Une fois qu'on l'ouvre, les conséquences sot inexorables »⋅

Aux États-Unis d'Amérique, il existe des mesures de protection de la vie privée pour les données médicales qui sont couvertes par la Loi sur la transférabilité et la responsabilité en matière d'assurance-santé. Pour l'instant, les résultats des tests génétiques des consommateurs ne sont pas couverts par l'_Health Insurance Portability and Accountability Act_ ([HIPAA](https://fr.wikipedia.org/wiki/Health_Insurance_Portability_and_Accountability_Act))

## Transférabilité, responsabilité, persistance et irréversibilité

Quand la police utilise cette puissance des données génétiques sans justice c'est déjà terrible et quand ces informations se retrouvent _fuitée_ c'est éminemment plus grave que de se faire voler son code de carte crédit ou un piratage de vos données bancaire. Ce patrimoine génétique vous l'avez en commun avec votre famille et avec des individus que vous ne connaissez pas. Ce code peut révéler énormément de votre état de santé sans que vous le sachiez. Lorsque vous donnez à quelque entité que ce soit votre matéreil biologique, c'est aussi le partimoine et les données de celles et ceux qui vous ont précedé, de celles et ceux qui viendront après vous. 

les données pourraient être utilisées pour faire de la discrimination génétique à l'égard de personne n'ayant comis aucun crime, comme le refus d'hypothèques ou l'augmentation des coûts d'assurance. À l'avenir, si les données génétiques deviennent suffisamment courantes, les gens pourraient être en mesure de payer des frais et d'avoir accès aux données génétiques de quelqu'un, de la même façon que nous le pouvons maintenant pour accéder aux antécédents d'une société. Laurent Alexandre, en France et dans une tribune sur le journal L'Express, propose même de « sélectionner les femmes les plus intelligentes pour les favoriser à la procréation »[^15]. Les cadenas de la boite de l'[eugénisme](https://fr.wikipedia.org/wiki/Eug%C3%A9nisme) souvrent en grand. C'est là la contrôle social sur le droit à enfanter, le contrôle social sur une pretendue hierarchie de l'intelligence. Pourquoi pas des sas et portiques de sécurité, comme dans La Zone de Dehors, avec des mâchoires, qui filtrent l'accès de personnes à des zones géographiques en fonction de leur ATGC, établissant des rangs sociaux au passage.

> « [...] _Confisquer le rapport à soi dans l'épaisseur d'un dossier jamais clos. Vous dire qui vous avez été, comment vous êtes et qui vous devrez être. Non pas mutiler, non pas opprimer ou réprimer l'individu comme on le crie si naïvement : le fabriquer. Le produire de toutes pièces et pièce à pièce. Même pas ex nihilo : à partir de vous-mêmes, de vos goûts, désirs et plaisirs ! Copie qu'on forme tout simplement._ » La Zone du Dehors, Alin Damasio, 1999

Un autre problème complique la question de ces bases de données génétiques : les tests auprès des consommateurs sont souvent erronés. Oui s'agit bien de consommer un service marchand et non pas de l'accés aux soins ou la justice. Les rapports peuvent donner de faux résultats positifs, et même les tests d'ascendance peuvent être très imprécis. Par exemple, certains tests de chez 23andMe ont été approuvés par la Food and Drug Administration (FDA), mais d'autres ne l'ont pas été, ce qui signifie que des résultats pourraient être inexacts.

Enfin, l'un problème les plus épineux est que l'on ne réecrit pas le Vivant et l'on ne fait disparaitre votre ADN. Il est même transmis en partie non négligeable de génération en génération. Il est possible d'effacer ou détuire une base de données informatique, ou d'en demander l'interdiction de collecte. L'information génétique, elle, est immuable : il est possible de changer les numéros de carte de crédit ou même les adresses, mais vos informations génétiques ne peuvent pas être changées. Et l'information biologique support à celle génétique est souvent partagée involontairement ou par défaut de connaissance de nos droits.

Les sciences et technique évoluent. En 1997, lorsque le médecin légiste australien Roland Van Oorschot a stupéfié le monde de la justice pénale avec un article de neuf paragraphes intitulé "DNA Fingerprints from Fingerprints". Il a révélé que l'ADN pouvait être détecté non seulement à partir de fluides corporels, mais aussi à partir de traces laissées par un toucher. Partout dans le monde, les enquêteurs ont commencé à fouiller les scènes de crime à la recherche de quoi que ce soit - une poignée de porte, un comptoir, un manche de couteau - qu'un auteur aurait pu tacher d'ADN "tactile" incriminant[^16].

Mais l'article de Van Oorschot contenait également une observation importante : L'ADN de certaines personnes est apparu sur des objets avec lesquels elles n'avaient jamais été en contact. 

## Revenons en France

Le droit français, la police française, la justice, ne fonctionnent pas comme USA. Nous avons pourtant nos _disruptions_ propres et notre boite de Pandore est commune. « le personnel de santé se transforme peu à peu en auxiliaire de police » dénonçait l'association des Médecins urgentistes lorsqu'en décembre 2018 les services de santé ont mis en partage des fiches des « gilets jaunes » blessés dans un fichier accessible au ministère de l'intérieur avec « bilans quantitatifs » obtenus à partir de ce fichier. Ce qui fut révelé par [Mathilde Goanec et Jérôme Hourdeaux dans Médiapart le 11 janvier 2019](https://www.mediapart.fr/journal/france/110119/les-services-de-sante-ont-fiche-des-gilets-jaunes-blesses?onglet=full). Pas de traces de prélèvements et biologique avec analyse génétique dans cette affaire pour l'instant. La relation de confiance, confier une intimité, est rompue, telle une grenade dans l'asile. Il y a maintenant matière à pousser des individus à ne pas se rendre dans une manifestation politique par crainte pour leurs intimités et libertés, ce qui est une forme de « contrôle préventif » dans le langage de certaine et certains politiques. Nous ne sommes pas encore au niveau des USA mais un sacré coup de pied de biche vient d'être mis dans la boite de Pandore. 

Après ce qui a été appelé « une serie d'affaires liées aux attentats indépendantistes bretons » de 1993 à 2000, un procès en cours d'assise en 2005 avait lieu à Paris. Pour L'affaire de Plévin sur le vol d'explosif, l'ADN d'un militant habitant à Rennes a été retrouvé sur un engin découvert devant un bureau de poste de cette ville. Avancée fulgarante pour la police et la justice. Une affaire d'ADN qui arrangeait bien des bords politiques aussi, mais une affaire ADN _mal gérée_[^17]. « _L'empreinte génétique de Jérôme Bouthier se révèle finalement avoir été faite sur un ADN mitochondrial, c'est-à-dire qu'elle peut appartenir à des centaines de personnes dans la seule région de Rennes_ ». Ce fut une bonne nouvelle pour la défense de l'accusé, une marque d'erreur forte pour la police, et un petit bout de code génétique de centaine de personnes ajouté à un fichier national. Il semble que dans ce cas, la _preuve génétique_ ait servi la cause de la pression politique sur la justice et qui, pour qui sait voir plus loin que la fumée, illustre la marche vers une fantastique machinerie.

En france assui des affaires criminelles anciennes sont résolues avec l'aide de traces biologiques et d'ADN, comme pour « Les disparues de Perpignan 1995 -2001 » en grande partie élucidée en 2015[^18]. En France, comme partout dans le monde, il est heureux que des supects soient confronté à leurs accusations, que des personnes reconnues coupables purgent leur peine, pour les victimes, pour les familles et proches de victimes, pour le fonctionnement de la société dans son arrangement actuelle.

Depuis les années 2000, la science et les techniques ont beaucoup évolué, la légilsation elle aussi a évolué, concernant les traces biologiques et les données génétiques. 

Par exemple, l’[article 16-11 du code civil](https://www.legifrance.gouv.fr/affichTexteArticle.do;jsessionid=1DFD1504ADB15E2A747920341BA21105.tpdjo05v_3?cidTexte=JORFTEXT000023707312&idArticle=LEGIARTI000023708917&dateTexte=20131210&categorieLien=id#LEGIARTI000023708917), de la loi n° 2011-267 du 14 mars 2011, reconnaît le droit aux particuliers à effectuer un test de paternité ou tout autre test ADN dans certains cas précis:

+ le cadre d’une procédure judiciaire pour faire établir un lien de filiation, contester un lien de filiation, soit pour l’obtention ou la suppression de subsides.
+ à des fins médicales ou de recherche scientifique
+ pour établir, lorsqu’elle est inconnue, l’identité de personnes décédées.

Pour faire un test de paternité légal en France, il faut saisir le juge du tribunal de grande instance, avec l’aide d’un avocat. Mais les élu⋅e⋅s travaillent et poussent...

> « _Pourquoi la France est-elle le seul pays européen refusant à ses ressortissants d’accès libre aux tests ADN de paternité, alors que dans ses pays voisins en Europe, le test de paternité peut-être légalement effectué sur la base du consentement écrit de ses participants ?_ » Joël Guerriau Sénateur, JO du senat, [Question écrite n° 06644n 30 mai 2013](https://www.senat.fr/questions/base/2013/qSEQ130506644.html).

Nous vivons aussi dans une France où les chiffres des arrestations et de garde à vue sont en hausse extrêmement importante. Dans ces situtations la prise d'empreintes digitales est quasi systématique et le prélèvement biologique est de plus en plus fréquent. 

Depuis 2003, l'Institut génétique Nantes Atlantique (IGNA), laboratoire privé en partie financé par la Compagnie financière Edmond de Rothschild, pratique et vend, à la justice française entre autres, des tests d'orientation géo-génétique. Cela signifie que cet institut cherche à partir d'analyse du génome à établir des critères raciaux et/ou éthniques. En 2008, Fabrice Arfi pour Médiapart révélait « Des juges contournent la loi pour utiliser des tests ADN "ethniques" »[^19]. Une nouvelle sur laquelle le journal l'Express donnait une parole de réponse au PDG d'époque de l'Institut, aussi expert auprès dela cours de Cassation, pour affirmer qu'il « aurait pris le soin de s'assurer "encore ce matin de la légalité du procédé auprès d'un juge et de ses avocats" »[^20]. Fichage racial et ethnique, double face expert aux juges et intérêts privés, négation de classement éthnique, les avancées si belles... le vice, la tromperie, la passion, l'orgueil, dans la même boîte. Et Marie-Gaëlle Le Pajolec, directrice de l'Institut génétique Nantes Atlantique (IGNA) en 2014 de renchérir dans le journal du Figaro[^21] en 2014 :

> « _La loi est impuissante à empêcher quiconque d'effectuer un test génétique. Alors, autant permettre de le réaliser dans un cadre réglementé qui garantirait la sécurité d'une double analyse, ce que de nombreux laboratoires étrangers s'épargnent pour limiter les coûts. Sans compter le manque à gagner pour leurs homologues français._ »

Toujours en Bretagne, dans l'affaire des _incendies de L'Armor Baden_, les gendarmes procédèrent au prélèvement de l'ADN de tous les hommes de la commune. Simon Tattevin, résident occasionnel pour le vacances, avait refusé de donner son ADN. Il avait envoyé le 23 mars une lettre ouverte<sup>[ref page 2](https://ldhsarlat.wordpress.com/2013/03/28/fichier-adn-celui-qui-refuse-le-prelevement-est-suspect)</sup> au procureur, accompagné du « Meilleur des Mondes », le livre d’anticipation d’Aldous Huxley.

> « _Les gendarmes sont passés en février chez mes parents demander mon ADN et celui de mon père, explique Simon Tattevin. Dans un premier temps, j’ai refusé de façon très intuitive, comme mon père, qui a ensuite changé d’avis. Puis les gendarmes m’ont rappelé deux semaines plus tard. Le ton cordial avait laissé place à un ton beaucoup plus agressif. Ils m’ont demandé ce que j’avais à me reprocher, m’ont rappelé que c’était un délit de refuser. Ils m’ont dit : “De toutes façons, si on veut votre ADN, on l’aura.”_ »

La loi en France autorise les enquêteurs à prélever et comparer l’empreinte génétique d’une personne lorsqu’il existe « une ou plusieurs raisons plausibles de soupçonner » qu’elle ait pu commettre une des infractions prévues. Mais dans ce cas, son ADN ne peut pas être conservé dans le Fichier national automatisé des empreintes génétiques (Fnaeg). « Le seul élément intéressant que nous ayons est un ADN retrouvé sur un des lieux et inconnu au Fnaeg, explique le procureur de Vannes. Les autres pistes n’ont rien donné. » D’où le prélèvement effectué sur « 350 à 400 » personnes. Une opération « plutôt exceptionnelle », reconnaît Thierry Phelippeau, mais justifiée selon lui par le fait qu’elle cible « un groupe quand même précis ». « Les témoignages et l’ADN recueillis confirment que l’auteur est sans doute un homme », explique-t-il dans [Mediapart du 27 mars 2013](https://www.mediapart.fr/journal/france/270313/fichier-adn-celui-qui-refuse-le-prelevement-est-suspect)

En cas de refus de prélèvement Biologique pour analyse ADN, le code pénal prévoit une sanction maximale d'un an d'emprisonnement et de 15 000 euros d'amende. L'[article 706-56](https://www.legifrance.gouv.fr/affichCode.do?idArticle=LEGIARTI000006577736&idSectionTA=LEGISCTA000006138132&cidTexte=LEGITEXT000006071154&dateTexte=20130620), qui détaille les crimes et délits qui peuvent donner lieu à prélèvement, montre que la palette est large : du meurtrier au simple receleur. Dans tous les cas de figure, quoiqu'il vous arrive, quelque soit la pression, ne donnez absolument rien avant d'avoir un⋅e avocat⋅e !

> « _Soit vous donnez votre ADN comme un suspect dans une enquête, soit vous refusez et devenez alors suspect dans cette même enquête. Un vrai ruban de Möbius._ » Mediapart

La France et les USA fonctionnent différemment mais ces deux pays, grandes démocraties actuelles, ont ouvert une boîte aux plaies des libertés et à la démocratie, comme beaucoup d'autres pays. Ces deux états ayant même des démarches qui pourraient être complémentaires et les maux de l'humanité, eux, n'ont pas de frontières. 

Le fichage, les données personnelles, d'autant plus celles moprhologiques, biologiques, généalogiques et genétiques, sont des parts de vous dont on vous prélève le secret. En ce sens, ce sont des morceaux d'intimité dont on vous prive. Elles n'appartiennent plus uniquement à vous et votre _lignée géntique_ et sont utilisées à des fins non déterminées par votre volonté. L’intimité était originellement possible dans les « asiles » : ce terme vient du latin _asylum_, « lieu inviolable, refuge » et du grec _asulon_ « qui ne peut être pillé ». Nous avons vu auparavant que ce _pillage_ peut s'effectuer sans pour autant que 95% de la population ait consenti à fournir un échantillon biologique. Il aussi très prondément institué un discours politique révelateur d'une nouvelle relation de la science et de la technique avec la pratique sociale, dans un monde où l'information est elle-même un produit de la technique. Un paradigme qui a une « incidence de la rationalité scientifique sur le "monde social vécu et ses répercussions sur le fonctionnement de la démocratie ». Ce que Jurgen Habermas décrit dans « La Technique et la science comme “idéologie” ».

Le discours ventant l'institution de la science et de la technique, privatisée au passage, comme solution unique et ultime pour sattaquer à la misére, à la vieillesse, à la maladie, aux crimes et à la guerre, à la folie... Ce discours ouvre une boîte plus que noire et retourne toutes résistances à nourrir son propore totalitarisme, la résistance devenant cause et raison originelle à son _inexorable marche vers la sécurité_.

> « _L'ironie de l'histoire monsieur Capt, veut que ce soit paradoxalement la lutte acharnée de gens comme vous, de révoltés épris de justice et de liberté, qui ait poussé les gouvernements à se remettre en cause, à affiner et à perfectionner sans cesse leur stratégie pour finalement édifier la plus fantastique machinerie de pouvoir jamais mise en œuvre : le contrôle._ » La Zone du Dehors, EDCBA, Alain Damasio ed. La Volte, Galmimard, folio SF, p.366.[^22]

Coupables alors que nous sommes, de l'ADN nous en déposons presque partout, du cheveux qui tombe naturellement à la goutte de sang en passant par la salive sur un verre ou l'éternuement et la transpiration, un doigt posé sur un bord de porte. Une personne moyenne peut perdre plus de 50 millions de cellules cutanées par jour. Erin Murphy, auteure de "Inside the Cell", un livre sur l'ADN médico-légal, a calculé qu'en deux minutes, une personne moyenne perd suffisamment de cellules cutanées pour couvrir un terrain de football (Un magnifique gisement de ressources à exploiter pour toute start-up écolo ESS éthique qui se respecte ?).
Nous laissons des traces de notre matériel génétique partout, même sur des choses que nous n'avons jamais touchées. Pendant ce temps des personnes dans des services privés ou publics font des rêgles et des collectes qui nourrissent un contrôle social. Dans le même temps où l'on nous _intègre_ biologiqument et socialement, parfois avec l'argument de notre sécurité, nous somme aussi stigmatisé⋅e⋅s (fichage et ségrégation) et opprimé⋅e⋅s (fichage et potentilatié de culpabilité). Dans le même temps ou nous contribuons gratuitement, travailleuse et travaillerus de notre panoptique, à construire nos propres prisons ; et nous alimentons un inconscient qui nous pousse déjà à oublier de vouloir d'agir dans de nombreuses situations, un peu comme un enfant qui ne mettra plus la main au feu une fois l'expérience de la brûlure vécue.

## Flou, failles et problèmes

Des chercheurs néerlandais ont testé 105 objets publics : mains courantes d'escalators, poignées de portes de toilettes publiques, poignées de paniers, pièces de monnaie[^23]. 91% contenaient de l'ADN humain, parfois celui d'une demi-douzaine de personnes. Même les objets qui nous sont intimes - les aisselles de nos chemises, disons - peuvent porter l'ADN d'autres personnes, ont-ils trouvé.

La nature itinérante de l'ADN a de sérieuses implications pour les enquêtes médico-légales. Après tout, si des traces de notre ADN peuvent se rendre sur une scène de crime que nous n'avons jamais visitée, ne sommes-nous pas tous des suspects possibles ?

L'ADN médico-légal a d'autres défauts : Des mélanges complexes de nombreux profils d'ADN peuvent être mal interprétés, les statistiques de certitude sont souvent très mal calculées et les robots d'analyse génétique ont parfois dépassé les limites de leur sensibilité.

À mesure que les progrès technologiques résolvent certains de ces problèmes, ils augmentent le problème du transfert de l'ADN. Chaque nouvelle génération d'outils médico-légaux est de plus en plus sensible ; les laboratoires d'aujourd'hui peuvent identifier les personnes ayant un ADN à partir d'une poignée de cellules seulement. Une poignée de cellules peut facilement migrer, comme dans le cas d'une machine à laver.

> « _Un survol des données scientifiques publiées, des entrevues avec des scientifiques de renom et un examen de milliers de pages de documents judiciaires et policiers associés à l'affaire Kumra ont permis de comprendre comment le transfert secondaire d'ADN peut miner la crédibilité de l'outil le plus fiable du système de justice pénale. Pourtant, très peu de laboratoires criminels dans le monde étudient régulièrement et de façon rigoureuse le transfert secondaire de l'ADN._ »

> «_Cela s'explique en partie par le fait que la plupart des médecins légistes estiment que l'ADN est le moindre des problèmes dans leur domaine. Ils n'ont pas tort : L'ADN est la science médico-légale la plus précise que nous ayons. Il a disculpé un grand nombre de personnes condamnées en se basant sur des disciplines plus imparfaites comme l'analyse des cheveux ou des marques de morsures. Et il y a eu peu de cas publiés d'ADN impliquant par erreur quelqu'un dans un crime._ » [The Marshall Project, 19/04/2018, sur l'affaire Lukis Anderson accusé d'un crime brutal qu'il n'a pas commis. ](https://www.themarshallproject.org/2018/04/19/framed-for-murder-by-his-own-dna).

M. Van Oorschot, chercheur en criminalistique dont l'article de 1997 a révolutionné le domaine, met en garde contre le fait de ne pas croire au pouvoir de l'ADN « tactile » pour résoudre des crimes. « Je pense que cela a eu un impact énorme et positif  », dit-il. « Mais personne ne devrait se fier uniquement à l'ADN pour juger de ce qui se passe. » 

Nous pouvons aussi prendre en considération quen personne, avec des intentions qui lui sont propres, déplace volontairement des traces biologique d'un autre individu d'un point A à un point B. Ou même l'interférence d'une mouche[^24]. En espérant que la boite de Pandore ne se soit pas refermée avant de nous avoir laissé libre l'Espoir, nous verrons également lors du [Fetival des Libertés Numériques à Rennes en Février 2019](https://xavcc.frama.io/fdln-2019/) comment nous pouvons brouiller ou effacer des traces biologiques et génétiques. 

Ces _histoires_ de traces biologiques et de fichier génétique s'intègrent dans système d'information, avec de la technologie, des processus, des personnes, comportements sociaux, et des structures organisationnelles, ainsi que dans un « cycle du vivant » plus vaste que nos tractations humaines. Les problèmes et les solutions qui s'y rattachent sont de notre responsabilité à toutes et tous et ne peuvent appartenir, ni peuvent être dirigés, par quelques uns. Il devient alors urgent d'interroger de travailler cela par le prisme des Communs[^25].

_Image d'illustration : Epimetheus opening Pandora's box, Giulio Bonasone (Italian, active Rome and Bologna, 1531–after 1576). Engragving. Public Domain_

_Des questions ? Des améliorations ? Des critiques ? Des corrections ? Des suggestions ? Contactez-moi via les moyens en page d'accueil de ce blog ou en ouvrant une [ISSUE](https://framagit.org/Xavcc/xavcc.frama.io/issues) pour travailler ensemble sur ce texte sous licence libre._

## Notes et références

[^1]: La page wikipedia du tueur du Golden State <https://fr.wikipedia.org/wiki/Golden_State_Killer>

[^2]: J'ai évoqué cette entreprise et exposé certains des problèmes et menaces qu'elle pose dans la conférence au Web2Day 2018 <https://xavcc.frama.io/biononymous-bioprivacy-research/#web2day-2018-nantes>

[^3]: « Plus de 92 millions de détails de compte du service de généalogie et de tests ADN MyHeritage ont été trouvés sur un service privé. » <https://www.theverge.com/2018/6/5/17430146/dna-myheritage-ancestry-accounts-compromised-hack-breach>

[^4]: source <https://www.sfchronicle.com/crime/amp/Arrest-of-suspected-Golden-State-Killer-through-12872258.php>

[^5]: Jai commencé en 2018 à documenter tout cela dans des pages wiki, ici en français <https://no-google.frama.wiki/g00gle:google-et-genetique>, ici en anglais <https://wiki.fuckoffgoogle.de/index.php?title=Google_Involvement_in_Genetics>

[^6]:Voir la section dédiée dans revue d'actualité Biononymous et Bioprivacy n°6 de Janvier 2019 <https://framastory.org/story/xavcc/biononymous-et-bioprivacy-6>

[^7]: l'article complte du Washington Post <https://www.washingtonpost.com/science/2018/10/11/even-if-youve-never-taken-dna-test-distant-relatives-could-reveal-your-identity>

[^8]: une partie importante de la revue d'actaulité Biononymous et Bioprivacy n°5 de Novembre 2018 est consacré a des risques et mesures sur ce débat <https://framastory.org/story/xavcc/biononymous-et-bioprivacy-5>

[^9]: communiqué officiel de réponse ferme par l'UNICEF Non, nous n'aiderons pas le gouvernement à ficher des enfants, 12 décembre 2018, <https://www.unicef.fr/article/non-nous-naiderons-pas-le-gouvernement-ficher-des-enfants>

[^10]: voir <https://www.cnil.fr/fr/fnaeg-fichier-national-des-empreintes-genetiques>

[^11]: Dans la dualité entre discours et action qui peut créer un dette et/ou illusion, je me souviens des Assises de la Médiation Nuémrique renommée Numérique en commun et des critiques qui en ont émergé <https://xavcc.frama.io/homo-spectaculus>

[^12]: voir <https://www.cnil.fr/fr/les-donnees-genetiques-premier-titre-de-la-nouvelle-collection-point-cnil>

[^13]: tableau de croissance du nombre de profils génétiques au FNAEG , source Wikipedia <https://fr.wikipedia.org/wiki/Fichier_national_automatis%C3%A9_des_empreintes_g%C3%A9n%C3%A9tiques#Statistiques>

[^14]: L'article complet de l'EFF qu imériterait selon moi un traduction en français <https://www.eff.org/fr/taxonomy/term/11283>

[^15]: Via twitter, accès à la photo de l'entière tribune publiée en Janvier 2018 <https://twitter.com/PatrickLavielle/status/959432108203757571>

[^16]: 1997 Nature Publishing Group Van Oorschot, R. A. H. & Jones, M. K. DNA fingerprints from fingerprints. Nature 387, 767 (1997). doi:10.1038/42 <https://www.nature.com/scitable/content/DNA-fingerprints-from-fingerprints-11782>

[^17]: L'affaire de Plévin, Bretagne, sur wikipedia <https://fr.wikipedia.org/wiki/Affaire_de_Pl%C3%A9vin#Rennes_et_Qu%C3%A9vert_:_l'%C3%A9rosion_progressive_de_l'accusation>

[^18]: À la mi-octobre 2014, l'ADN de l'agresseur de Mokhtaria Chaïb a été identifié comme étant celui de Jacques Rançon, cinquante-quatre ans, père de quatre enfants (issus de deux liaisons différentes), habitant à Perpignan depuis 1997, année même du meurtre de Mokhtaria <https://fr.wikipedia.org/wiki/Meurtres_de_la_gare_de_Perpignan>

[^19]: L'article complet est disponible ici <https://www.mediapart.fr/journal/france/280508/des-juges-contournent-la-loi-pour-utiliser-des-tests-adn-ethniques>

[^20]: l'arcile de référence <https://www.lexpress.fr/actualite/societe/justice/des-tests-adn-ethniques_505172.html>

[^21]: Entre 2006 et 2014 dans le Figaro Marie-Gaëlle Le Pajolec est citée ou interviewée 5 fois en qualité d'experte <http://recherche.lefigaro.fr/recherche/%20Marie-Ga%C3%ABlle%20Le%20Pajolec/>

[^22]: La Zone du Dehors est paru pour la première fois aux Éditions CyLibris le 4 avril 1999. 

[^23]: « [..] Des expériences en machine à laver montrent que le transfert et la persistance pendant la lessive sont limités pour l'ADN et dépendent du type de cellule pour l'ARN. Les affections cutanées telles que la présence de sébum ou de sueur peuvent favoriser le transfert de l'ADN. Les résultats de cette étude, qui comprend 549 échantillons, nous permettent de mieux comprendre la prévalence du matériel cellulaire humain dans les scénarios de fond et d'activité.[..] » L'abstract et informations de l'étude sont disponibles ici <https://www.sciencedirect.com/science/article/abs/pii/S1872497315301083>

[^24]: La conclusion du « RAPPORT SUR LES QUESTIONS SCIENTIFIQUES DANS L'AFFAIRE DE OKLAHOMA V. DANIEL K. HOLTZCLAW PAR UNE ORGANISATION INTERNATIONALE GROUPE D'EXPERTS MÉDICO-LÉGAUX » évoque cette erreur, parmi d'autre problème de procédure sceintifique puis judiciaire <https://michellemalkin.com/wp-content/uploads/2018/02/DNA.pdf>

[^25]: Les réflexions, les travaux et défintions de Lionel Maurel me semblent aujourd'hui être l'une des meilleures approches pour aborder les communs et _Biononymous & Bioprovacy_ <https://scinfolex.com/?s=communs>. Je laisse aux lectrices et lecteurs le soin et les libertés de trouver une approche des Communs qui conviendraient. 