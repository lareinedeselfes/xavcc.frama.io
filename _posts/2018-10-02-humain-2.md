---
title: "Qu'est-ce que l'Humain ? 2/5"
layout: post
date: 2018-10-02 14:30
image: /assets/images/humman-face.jpg
headerImage: true
tag:
- hsociety
- Rennes
start: true
category: blog
author: XavierCoadic
description: Arpentage vers ce qui a fait de nous une humanité
---

Se questionner sur ce que nous sommes n'est pas une lubie, ni un effort dont nous pourrions faire l'économie. D'ailleurs, l'invocation quasi permanente dans des discours d'un « humain » remis au centre de telle technologie, de tel processus , de telle organisation, devrait nous alarmer pour deux simples raisons. La première étant que plus un concept est répété à tue-tête plus la personne ou l'organisation éméttrice de cette invocation a de chance de vouloir vous hypnotiser, certainement pour vous faire avaler une poudre de Perlimpinpin. Dès ce moment une sonnette d'alarme devrait rententir en chacune et chacun de nous. La seconde raison est encore plus simple, si le concept est répété comme [un écho hypnotique](https://xavcc.github.io/homo-spectaculus), sans jamais proposer de questionnement et de définition de ce concept même, il est sain pour la personne réceptrice du message de demander une réponse travaillée à la question « qu'est ce que l'Humain ». Ces deux raisons fonctionnent pour presque tous les concepts martelés, essayez avec des complaintes _blockchain_ par exemple. 

Essayons-nous d'arpenter ce qui pourrait faire de nous des humains. Il y a dans ce cheminement une probabilité non négligeable d'occurrence de ressources et de preuves qui remettront en cause un prosélytisme centrant une vision sur un concept qui n'a souvent pas été défini par le « zèle » déployé afin de rallier des personnes à un dogme.

Après [un premier texte sur cette question de l'Humain](/humain) qui abordait les arpents de notre possible humanité par la biologie, la neuro-biologie ou encore le langage, cette seconde partie se veut plus orientée par la paléoanthropologie.

Ainsi je vous propose dans ces lignes de partager une interprétation sur la question qu’est-ce que l’humain, notamment, mais sans m’y restreindre strictement, par des sources provenant de Jean-Dider Vincent, neurobiologiste ; de Michel Serres, philosophe ; de Pascal Picq, paléoanthropologue.

Pareil à toutes les autres espèces le genre homo est l'aboutissement des objets dits vivants au sein de notre planète. Les sciences nous permettent aujourd'hui de (re)tracer avec sérieux l'arbre généalogique de notre espèce. Il y a 6 millions d'années, cette ligne d'études fait apparaître un ancêtre commun avec les chimpanzés ; il a 70 millions d'années c'est un ancêtre commun avec les autres primates ; 200 millions d'années avec les mammifères ; 250 millions d'années avec les reptiles ; 400 millions d'années avec l'ensemble des vertébrés ; 1,5 milliards avec les algues et plus de 3 milliards d'années avec les premières bactéries. Ne ne sommes presque rien dans le temps de l'évolution du vivant, ce temps étant lui-même une infime partie de la grande intégrale de l'univers.

![](/assets/images/evol.png)

## Paléoanthropologie et humains

Il y a à peine plus de 60 ans, les questions de l'évolution de l'homme, elles étaient posées ainsi et pas autrement, étaient de la spécialité des préhistoriens et préhistoriennes. Sur une projection stricte, la préhistoire est l'étude des cultures avant l'invention de l'écriture. Cela induit que pour la période pré-écriture l'_homme_ est défini par la culture de l'outil en pierre taillée. C'était une affirmation confortable, que l'_homme est l'outil_, qui éludait nombre de questionnements et nous installait d'un coté d'une frontière, renfermant _l'homme_ sur ces productions, avec le singe. 

L'interrogation du **phénomène humain**, c'est à dire est un processus, un fait du monde physique (objet, action…), psychique (émotion, pensée…) ou social (produit d'interactions sociales) qui se manifeste lui-même, n'est qu'un travail récent dans cette configuration. 

La paléoanthropologie est la science qui étudie l'évolution biologique. Cette discipline ce dégage de celle de la préhistoire à l'époque où l'on réalise que nos origines du genre homo sont africaines, lorsque nous comprenons dans les années 1960 que l'évolution biologique précède l'évolution culturelle. Un changement d'intitulé qui ouvrira un changement de paradigme majeur : la recherche d'une définition de _l'homme_, du genre homo, au sens biologique du terme. La paléoanthropologie est alors à la charnière de la théorie de l'évolution et de la préhistoire.

> « _Le propre de de l'humain n'est-il pas justement de se poser cette question "Qu'est ce que l'Humain ?". Et est-ce propre à notre espèce Homo Sapiens ?_ » Pascal Picq, Qu'est-ce que l'humain, éditions le pommier 2003.

Les premiers Hommes en Afrique il y a 3 millions d'années se posaient-ils la question de la condition humaine ou du phénomène humain ? Il est encore aujourd'hui difficile de répondre à cette énigme. Se posaient-ils la question à chaque descente d'arbre ? Dans ces questions niche un noyau de confusion dans le désir de définir l'Homme comme un primate humain, puisque dans ce cas de figure l'humain n'est toujours pas défini. Alors qu'au sens strict, l'évolution biologique, c'est à dire l'hominisation, processus d'évolution du genre homo, permet de forger le concept d'humanisation, et donc de travailler cette nouvelle matière d'étude.

Alors de quoi émerge _l'humain_ ? Cette nouvelle question porte désormais à deux le nombre d'interrogations auxquelles répondre. Au début des années 2000, deux évidences étaient prises en défaut de points ennuyeux par Pacal Picq. Le première était que notre espèce avait reçu par un phénomène de génération spontanée une faculté unique, « ramenée par certains paléoanthropologues à quelques mutations aussi improbables que bénies. » (SIC). La seconde était la thèse de [Teilhard de Chardin](https://fr.wikipedia.org/wiki/Pierre_Teilhard_de_Chardin) postulant à la tendance interne de la vie à mener inexorablement à un état de conscience. Pour Picq, l'ennuyeux, voir le fâcheux, dans ces deux évidences est qu'il est impossible de « _situer la question dans les sciences_ », autrement formulé de tester ce qu'est l'humain, et de le définir.

La question de la discontinuité ou de la continuité entre l'animal, l'_Homme_ et l'_Humain_ semble pertinente aujourd'hui. Au regard du seuil d'émergence du genre Homo, sur lequel nous retrouvons une grande diversité de fossiles dégageant des critères anatomiques propres au genre humain, qui vient s'entremêler avec les preuves de comportements des grands singes, frères d'évolutions, comme similitudes avec nos propres comportements, cette question est en tension. 

<iframe src="https://mamot.fr/@XavCC/100775492470585789/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400"></iframe><script src="https://mamot.fr/embed.js" async="async"></script>

## D'où vient l'humain ?

La pensée grecque posait il y a 2 500 ans des questions sur ce _qu'est l'humain_. Pascal Picq, dans « Qu'est ce que l'humain », rappelle un échange entre Diogène et Platon. Le dernier affirmant que l'homme est un bipède sans poil. Diogène revint ainsi dans l'arène jetant un poulet déplumé pour s'esclaffer « Voici l'homme de Platon ! ».
Depuis, grâce aux sciences, nous avons appris qu'il existe beaucoup d'espèces d'hommes sur l'histoire de notre Terre. Nous découvrons encore aujourd'hui les mœurs de bipèdes comme les bonobos et chimpanzés comme proches, voir similaires, des nôtres.

Nous sommes depuis l'apparition de la paléoanthropologie dans le paysage des sciences face aux enjeux d'une triple « fonction », ou « enquête de fonction », dans l'équation de la question qu'est ce que l'humain :

+ De diagnostic : qui identifie l'état et les symptômes
+ De pronostic : qui évalue son évolution
+ De philothérapeutique : qui prévient les risques de cette enquête comme les risques de processus de l'humain lui-même.

> « _En paléoanthropologie l'humain est un concept qui va de soi_ » Pascal Picq, Qu'est ce que l'humain, éditions Le Pommier 2003.

Le concept d'humain est difficile à manipuler.

> « _Souvent les anthropologues confondent sans discernement l'homme et l'humain_ » Pascal Picq, 2003.

Ensuite, lorsque les sciences « humaines » rencontrent les sciences « naturelles » arrive la juxtaposition du concept naturaliste, l'espèce et le genre, avec le concept philosophique. Ce qui est risqué comme empilement. Cela peut se traduire par un anthropocentrisme notamment, sans classement valide ou alors très peu, au sens arborescence biologique, de dénomination de _primates non humains_ et du seul _primate humain_, l'homme. La seule définition de l'homme apparaît alors entre les lignes comme « celui sauvé du singe ». Étonnante marche de terminologie ou acte de conviction semblable à l'acte de foi ?

Nous n'avons jamais été modernes, titrait [Bruno Latour en 1991](https://fr.wikipedia.org/wiki/Bruno_Latour). Diogène et Platon dans leurs questionnements et critiques se tordraient au sol face aux évidences avec peu ou prou de critères travaillés en profondeur. Car face aux grands singes, dans la triple enquête de fonction, nous semblons n'être que la continuité. Cela se profile dans l'ensemble des travaux des évolutionnistes, des éthologues, des zoologistes, des naturalistes, qui travaillent la comparaison de l'homme et de l'animal. 

> « _Alors d'où vient l'humain ? Dans l'avancée des connaissances il reste l'héritage de Descartes, qui a ouvert le chemin de la compréhension des structures, mais qui invoque un supplément d'âme pour l'humain_ »

Sommes-nous les seuls organismes vivants dotés d'une âme ? Ce pronostic évolutif est-il toujours valable aujourd'hui ?

## L'Homme n'est pas au centre

Nous, Homo sapiens, savons aujourd'hui que nous ne sommes ni plus forts, ni plus nombreux, ni plus résistants, ni plus longuement présents sur Terre, que d'autres espèces. Ce qui pourrait nous distinguer n'est pas ce que la grande intégrale cosmique raconte de nous mais le fait que nous avons commencé à lire cette intégrale, et ensuite à lire la petite équation dans cette intégrale dont nous sommes une branche. 

Depuis la _Systema Naturea_ de [Carl von Linné](https://fr.wikipedia.org/wiki/Carl_von_Linn%C3%A9) en 1758 nous sommes des primates, mot qui signifie « les premiers ». À présent notre carte d'identité naturaliste s'est enrichie. Nous sommes du genre _Homo_, dans la famille des hominidés, au sein de la super famille des homonoïdes, classés dans l'infra-ordre des enthorpoïdes ; dans les primates, parmi les _Archontas_ qui signifie « les chefs ». Nous nous sommes auto affublés d'être les « chefs premiers » dans un arbre de vie qui nous dépasse de toute part.

La science de classification des espèces est la systémique. À l'époque de Linné nous étions voisins très proches des orangs-outans et des chimpanzés en appuyant cette proximité par une conception de la nature comme figée par la Création. Alors, dans cette considération, si les grands singes ressemblent aux hommes c'est que le créateur l'a voulu ainsi. En 1809, [Jean-Baptiste Lamarck](https://fr.wikipedia.org/wiki/Jean-Baptiste_de_Lamarck) dans « Philosophie Zoologique » ouvre le concept de transformation des espèces. C'est à ce moment que l'on comprend que l'homme descend du singe. Cela est perçu comme un danger pour la définition de l'humain. C'est après deux siècles de conflit _scientifique_, [Thomas Huxley](https://fr.wikipedia.org/wiki/Thomas_Henry_Huxley), avec notamment « De la place de l'homme dans la nature » et Charles Darwin installent les gorilles et les chimpanzés plus près de l'homme que des autres singes dans l'arbre du vivant.

Systématiciens et naturalistes s'évertuerons à faire acte de création d'une classe à part pour l'homme dans les lignes de l'évolution avec des _caractères exclusivement humains_ en référence à la libération de la main et à la bipédie. D'un autre penchant psychologique d'autres mettent en création les caractères psychologiques et moraux propres à l'homme. C'est le cas par exemple du petit fils de Thomas Huxley, [Julian Huxley](https://fr.wikipedia.org/wiki/Julian_Huxley) théoricien de l'eugénisme. Cela servira de forge d'une classe à part nommée _[Psyhoza](http://edu.mnhn.fr/mod/page/view.php?id=366&lang=en)_. Cette opposition Hommes/bêtes au sein des animaux est aujourd'hui généralement considérée comme anthropocentrique, elle dominera la pensée évolutionniste jusqu'en 1977. Cela sous-entendait que si le corps de l'homme a évolué, ce qui constitue l'humain échappe à l'évolution. 

Les disciplines de la paléoanthropologie et de la préhistoire se développent entre la fin XIXe et du XXe siècle, période de vie de Darwin et [Stephen Jay Gould](https://fr.wikipedia.org/wiki/Stephen_Jay_Gould). Pendant cette période, les connaissances sur notre passé évolutif se font _protéger de_  tout relatif au singe en ce qui concerne la parenté. Les scénarios s'écrivent sans connaissance des grands singes et des autres singes. Il est admis que les caractéristiques prêtées comme évidence de l'humain telle que la bipédie, l'outil, la chasse, la culture, la vie sociale, se sont acquises lors de l'avancée de nos lointains ancêtres dans la savane sous pression de dangers. C'est une approche cartésienne du corps qui renferme l'âme, d'un processus humain qui redresse le corps pour ensuite faire l'acquisition d'items qui lui seraient propres et uniques.

Dans les années 70 la systématique moléculaire et la systématique phylogénétique viennent révolutionner cette approche. Cela advient après la découverte de la structure en double hélice de l'ADN en 1953 par Francis Crick et James Watson. En parallèle à L'institut Pasteur, François Jacob, André Lwoff et Jacques Monod mettaient en évidence le rôle de l'[opéron lactose](https://fr.wikipedia.org/wiki/Op%C3%A9ron_lactose) dans la génétique de l'_Escherichia coli_ qui apporte des réponses à la question fondamentale posée aux sciences du vivant, la compréhension des mécanismes d'hérédité. 
On compare  le matériel génétique avec la systémique moléculaire, hybridation puis séquençage de l'ADN, comparaison de chromosomes, pour établir des relations de lien de parenté. Avec la systémique génétique cesse le classement par une forme d'idée anthropocentrique mais en fonction des liens de parentés. Nos origines s'affirment africaines car chimpanzés et bonobo se retrouvent rangés plus près de l'homme dans l'arbre de la vie que des autres grands singes, comme les orangs-goutans entre autres. La famille des hominidés est alors composée des gorilles, des chimpanzés, des bonobo et des hommes. 

Nous savons aujourd'hui que nous partageons avec nos cousins les plus récents l'immense majorité de notre matériel génétique. Si de l'humain il y a, alors il n'est même plus au centre de ses propres considérations mais une infime partie dans un temps relativement court qui prend racine il y a environ 6 millions d'années dans notre _séparation_ avec notre très proche cousin le chimpanzé. 

La question « Qu'est ce que l'humain ? » n'est pas encore résolue car si l'_Homme_ des origines pourra être identifié, nous le verrons dans le prochain volet de cette série, sa définition reste une équation complexe.

## Remerciements

+ Émilie Picton pour relecture et corrections via [github)](https://github.com/XavCC/xavcc.github.io/tree/master/_posts)

### Images d'en tête

Photokinetic bacteria can be arranged into complex and reconfigurable patterns, including portraits, using a digital light projector. Human Face : <https://elifesciences.org/articles/36608>, Giacomo Frangipane, Dario Dell'Arciprete, Serena Petracchini, Claudio Maggi, Filippo Saglimbeni, Silvio Bianchi, Gaszton Vizsnyiczai, Maria Lina Bernardini, Roberto Di Leonardo  **Università di Roma "Sapienza", Italy; Institute of Nanotechnology (NANOTEC-CNR), Italy**
